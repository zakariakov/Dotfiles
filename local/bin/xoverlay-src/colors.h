#ifndef COLOR_S_HH
#define COLOR_S_HH

//rgba to float r/255, g/255, b/255, a/255
//Progress color  float r, g, b
#define PROGRESS 0.125,0.274,0.564
//Backround color  float r, g, b, a
#define BACKGROUND 0.086, 0.098, 0.145, 0.60

//#define CADET_GREAY "#91A3B0"
//#define MIKED_ORANGE "#DE621B"
//#define DWM_CYAN "#005577"
//#define BURGUNDY_RED "#5e000e"

//#define SOFT_RED "#D90A87"
//#define SOFT_RED_INTENSE "#AD12BE"

//#define COLOR_ACCENT SOFT_RED
//#define COLOR_ACCENT_INTENSE SOFT_RED_INTENSE

#endif
