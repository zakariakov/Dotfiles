#!/usr/bin/env bash

# You can call this script like this:
# $ ./brightnessControl.sh up
# $ ./brightnessControl.sh down

# Script inspired by these wonderful people:
# https://github.com/dastorm/volume-notification-dunst/blob/master/volume.sh
# https://gist.github.com/sebastiencs/5d7227f388d93374cebdf72e783fbd6a
BC_ICON="$HOME/.local/bin/xoverlay/brightness.png"

function get_brightness {
  xbacklight -get | cut -d '.' -f 1
}

function send_notification {
  icon=$BC_ICON
  brightness=$(get_brightness)
  # Make the bar with the special character ─ (it's not dash -)
  # https://en.wikipedia.org/wiki/Box-drawing_character
  #█████ ███░░ 
  #bar=$(seq -s "" 0 $((brightness / 5)) | sed 's/[0-9]//g')
  #barin=$(seq --separator="" 0 "$((20-${#bar} ))" | sed 's/[0-9]//g')
 # Str=
#	icon=${Str:${#bar}:1}

  # Send the notification
  #dunstify -i "$BC_ICON" -r 21392 -u low "$brightness%  $bar$barin"
 dunstify -a "Brightness" -i "$BC_ICON" -r 21392 -u low "" -h int:value:$brightness 
 # dunstify -r 21392 -u low -i "!" \
 #   "<span font='Awesome 64'>$icon</span>" "$bar$barin\n$brightness %\n" \
 #   -h string:bgcolor:#b5cbd5 -h string:fgcolor:#1f172c 

}

case $1 in
  up)
    # increase the backlight by 5%
    xbacklight -inc 5
    send_notification
    ;;
  down)
    # decrease the backlight by 5%
    xbacklight -5
    send_notification
    ;;
esac
