#!/usr/bin/env bash

# this script require 'bc'

ARG=$1
#old 1
#ZOOM=$(hyprctl getoption misc:cursor_zoom_factor | awk 'FNR == 3 {print $2}')
#old 2
#ZOOM=$(hyprctl getoption misc:cursor_zoom_factor |  awk '/^float:/{print $2}')

ZOOM=$(hyprctl getoption cursor:zoom_factor |  awk '/^float:/{print $2}')

MAX=3.0
MIN=1.0

echo "Zoom factor="$ZOOM

_ZOOM_IN(){
  Greater=$(echo "$ZOOM >= $MAX" | bc)

  if [[ $Greater -eq 1 ]]; then
     # hyprctl keyword misc:cursor_zoom_factor $MAX
      hyprctl keyword cursor:zoom_factor $MAX
      exit
  fi
 # hyprctl keyword misc:cursor_zoom_factor "$(echo $ZOOM | awk '{print $1 + 0.1}')"
  hyprctl keyword cursor:zoom_factor "$(echo $ZOOM | awk '{print $1 + 0.1}')"

}

_ZOOM_OUT(){
  Less=$(echo "$ZOOM <= $MIN" | bc)

  if [[ $Less -eq 1 ]]; then
   # hyprctl keyword misc:cursor_zoom_factor $MIN
     hyprctl keyword cursor:zoom_factor $MIN
    exit
  fi
  #hyprctl keyword misc:cursor_zoom_factor "$(echo $ZOOM | awk '{print $1 - 0.1}')"
  hyprctl keyword cursor:zoom_factor "$(echo $ZOOM | awk '{print $1 - 0.1}')"
}

case $ARG in
  zoomin) _ZOOM_IN ;;
  zoomout) _ZOOM_OUT ;;
  zoomfit)
  # hyprctl keyword misc:cursor_zoom_factor 1.0
   hyprctl keyword cursor:zoom_factor 1.0

  ;;
esac
exit

#hyprctl keyword misc:cursor_zoom_factor "$(hyprctl getoption misc:cursor_zoom_factor | grep float | awk '{print $2 + 0.1}')"
