#export LANG=en_US.UTF-8
#export LANGUAGE="en_US:ar_DZ"
export LANG=ar_DZ.UTF-8
export LANGUAGE=ar_DZ:en_US:ar
#export LC_ALL=ar_DZ.UTF-8
#export LC_NUMERIC=ar_DZ.UTF-8
#export LC_TIME=ar_DZ.UTF-8
#export LC_MONETARY=ar_DZ.UTF-8
#export LC_PAPER=ar_DZ.UTF-8
#export LC_NAME=ar_DZ.UTF-8
#export LC_ADDRESS=ar_DZ.UTF-8
#export LC_TELEPHONE=ar_DZ.UTF-8
#export LC_MEASUREMENT=ar_DZ.UTF-8
#export LC_IDENTIFICATION=ar_DZ.UTF-8
#export LC_COLLATE=C

export GSETTINGS_SCHEMA_DIR=/usr/share/glib-2.0/schemas/
export EDITOR=/usr/bin/vim
#export PAGER="most"
export TERMINAL="kitty"
export BROWSER='firefox'
export TERM=kitty

export PATH="${PATH}:${HOME}/.local/bin/"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"

if [ -n "$GTK_MODULES" ]; then
    GTK_MODULES="${GTK_MODULES}:appmenu-gtk-module"
else
    GTK_MODULES="appmenu-gtk-module"
fi
export GTK_MODULES

export QT_QPA_PLATFORMTHEME=qt5ct

if [  "$DESKTOP_SESSION" = "plasma"  ];then
	echo "kde"
elif [  "$XDG_CURRENT_DESKTOP" = "Wayfire"  ];then
	export MOZ_ENABLE_WAYLAND=1  
        export QT_QPA_PLATFORM=wayland
else    
	export QT_QPA_PLATFORMTHEME=qt5ct
 

	# if [  "$DESKTOP_SESSION" = "i3"  ];then
	#  	export  XDG_CURRENT_DESKTOP="i3"
	# elif [  "$DESKTOP_SESSION" = "bspwm"  ];then
 	# 	export  XDG_CURRENT_DESKTOP="bspwm"
  	# elif [  "$DESKTOP_SESSION" = "awesome"  ];then
 	# 	export  XDG_CURRENT_DESKTOP="awesome"
  	# elif [  "$DESKTOP_SESSION" = "openbox"  ];then
       	#  	export  XDG_CURRENT_DESKTOP="openbox"
  	# fi 
	
fi



