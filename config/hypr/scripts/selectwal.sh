#!/bin/env bash
ARG=$1
WAL_DIR=$HOME/wallpaper
[ -z $WAL_DIR ] && WAL_DIR=$HOME/.config/hypr/wallpapers

MONITOR1="eDP-1"
MONITOR2="HDMI-A-1"

ROFI_THME=~/.config/rofi/themes/config-wallpaper.rasi
cdialog() {
    yad --title='Confirm?' --borders=15 --center --fixed --undecorated --button=Yes:0 --button=No:1 --text="Do we want to apply a wal theme??" --text-align=center
}

image_blur() {
    _W=$(hyprctl -j monitors | jq '.[] | select(.focused==true) | .width')
    _H=$(hyprctl -j monitors | jq '.[] | select(.focused==true) | .height')
    magick $FILE -resize $_Wx$_H -blur 0x8 $HOME/.cache/blured.png
    ln -s -f "${FILE}" $HOME/.cache/background
}
## Select wall by rofi ----------------
#
selectWall() {

    FILE=$(find "$WAL_DIR" -type f \( -iname "*.jpg" -o -iname "*.jpeg" -o -iname "*.png" \) | while read rfile; do
        #   title=$(echo "$rfile" | awk -v r="" -v t="$WAL_DIR" '{gsub(t, r); print}')
        echo -en "${rfile}\x00icon\x1f${rfile}\n"
    done | rofi -show -dmenu -theme $ROFI_THME)

    # title=$(echo $FILE/"$WAL_DIR"/"")

    echo "FILE:" $FILE
    # FILE=${WAL_DIR}${FILE}
    if [ ! -f "$FILE" ]; then
        echo "No wallpaper selected"
        exit
    fi

    swww img "$FILE" --transition-type random \
        --transition-fps 60 \
        --transition-duration 2

    image_blur
    #============================
    cdialog
    if [[ "$?" == 0 ]]; then
        wal -q -i $FILE
        ~/.config/hypr/settings/change_colors.sh "wal"
    fi

    # sleep 0.5
    # nohup ~/.config/hypr/scripts/statusbar r >/tmp/waybar.log &
    # swaync-client -rs

}

## randomize wall from path -----------
randomWall() {
    FILE=$(find $WAL_DIR -type f \( -iname '*.PNG' -o -iname '*.JPG' -o -iname '*.JPEG' -o -iname '*.webp' \) | shuf -n 1)

    [ ! -f $FILE ] && FILE=$HOME/.cache/background

    Size=$(identify $FILE | awk '{print $3}')

    WW=$(echo $Size | cut -d "x" -f1)
    HH=$(echo $Size | cut -d "x" -f2)
    #  echo $WW $HH

    if [[ ("$WW" -le "800") || ("$HH" -le "500") ]]; then
        #  if ($WW <= 800) || ($HH <= 500); then
        echo "small image:  $WW $HH"

        swww img --no-resize "$FILE" --transition-type random \
            --transition-fps 60 \
            --transition-duration 2
    else

        swww img "$FILE" --transition-type random \
            --transition-fps 60 \
            --transition-duration 2
    fi

    # Monitor 1
    #swww img -o $MONITOR1 "$FILE" --transition-type random \
    #        --transition-fps 60 \
    #        --transition-duration 2
    #    ## monitor2 if exist
    #   NB_MONITORS=$(hyprctl monitors -j | jq length)
    #   if [ "$NB_MONITORS" -eq "2" ]; then
    #       FILE2=$(find $WAL_DIR -type f \( -iname '*.PNG' -o -iname '*.JPG' -o -iname '*.JPEG' -o -iname '*.webp' \) | shuf -n 1)
    #       [ ! -f $FILE ] && FILE=$HOME/.cache/background
    #       swww img -o $MONITOR2 "$FILE2" --transition-type random \
    #           --transition-fps 60 \
    #           --transition-duration 2
    #   fi
    image_blur
}

## restore preveious wall -------------
#
restorWall() {
    FILE=$(swww query | grep eDP-1 | cut -d ':' -f5)
    if [ ! -f $FILE ]; then
        FILE=$HOME/.cache/background
    fi
    swww img $FILE
}

case $ARG in
--random) randomWall ;;
--select) selectWall ;;
--restor) restorWall ;;
esac

exit
