#!/bin/env bash
cmd=neo --charset=arabic -s -D -S 5 -b 2
kitty -c ~/.config/kitty/kittymatrix.conf --class Matrix --start-as=fullscreen \
neo --charset=bin -s -D -S 5 -b 2  &
sleep 0.5
hyprlock
sleep 0.2
ClassName=$(hyprctl activewindow | grep initialClass | cut -d: -f2)
if [[ $ClassName == *"Matrix"* ]]; then
        hyprctl dispatch killactive
fi
