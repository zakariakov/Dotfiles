#!/usr/bin/env bash

APP_CLASS=$1
ICON=$2
WAYCONS=$HOME/.config/waybar/icons

function find_icon() {
    #  echo "path:" $1
    if [ -d $1 ]; then
        #echo "path:" $1
        RESULT=$(find -L $1 -name "${ICON}*" | sort -R | head -1)

        if [ ! -z $RESULT ]; then
            echo $RESULT
            echo $APP_CLASS
            # ln -s -f $RESULT $WAYCONS/$ICON
            exit
        fi
    fi
}

_findIconPath() {
    THEME=$(gsettings get org.gnome.desktop.interface icon-theme | tr -d "'")
    #THEME=McMojave-circle
    HOME_THEME_PATH=$HOME/.icons
    LOCAL_THEME_PATH=$HOME/.local/share/icons
    USERS_THEME_PATH=/usr/share/icons
    HICOLOR_THEME_PATH=/usr/share/icons/hicolor
    # ---- $HOME/.icons/
    find_icon $HOME_THEME_PATH/$THEME/apps/scalable
    find_icon $HOME_THEME_PATH/$THEME/apps/128x128
    find_icon $HOME_THEME_PATH/$THEME/apps/64x64
    find_icon $HOME_THEME_PATH/$THEME/apps/48x48
    find_icon $HOME_THEME_PATH/$THEME/32x32
    # ---- $HOME/.local/share/icons/
    find_icon $LOCAL_THEME_PATH/$THEME/apps/scalable
    find_icon $LOCAL_THEME_PATH/$THEME/apps/128x128
    find_icon $LOCAL_THEME_PATH/$THEME/apps/64x64
    find_icon $LOCAL_THEME_PATH/$THEME/apps/48x48
    find_icon $LOCAL_THEME_PATH/$THEME/32x32
    # ---- /usr/share/icons/
    find_icon $USERS_THEME_PATH/$THEME/apps/scalable
    find_icon $USERS_THEME_PATH/$THEME/apps/128x128
    find_icon $USERS_THEME_PATH/$THEME/apps/64x64
    find_icon $USERS_THEME_PATH/$THEME/apps/48x48
    find_icon $USERS_THEME_PATH/$THEME/32x32
    # ----INHHRETS THEME
    INHHRETS=$(cat $HOME_THEME_PATH/$THEME/index.theme | grep Inherits | cut -d "," -f1 | cut -d "=" -f2)
    #echo "INHHRETS:" $INHHRETS
    # ---- $HOME/.icons/
    find_icon $HOME_THEME_PATH/$INHHRETS/apps/scalable
    find_icon $HOME_THEME_PATH/$INHHRETS/apps/128x128
    find_icon $HOME_THEME_PATH/$INHHRETS/apps/64x64
    find_icon $HOME_THEME_PATH/$INHHRETS/apps/48x48
    find_icon $HOME_THEME_PATH/$INHHRETS/32x32
    # ---- $HOME/.local/share/icons/
    find_icon $LOCAL_THEME_PATH/$INHHRETS/apps/scalable
    find_icon $LOCAL_THEME_PATH/$INHHRETS/apps/128x128
    find_icon $LOCAL_THEME_PATH/$INHHRETS/apps/64x64
    find_icon $LOCAL_THEME_PATH/$INHHRETS/apps/48x48
    find_icon $LOCAL_THEME_PATH/$INHHRETS/32x32
    # ---- /usr/share/icons/
    find_icon $USERS_THEME_PATH/$INHHRETS/apps/scalable
    find_icon $USERS_THEME_PATH/$INHHRETS/apps/128x128
    find_icon $USERS_THEME_PATH/$INHHRETS/apps/64x64
    find_icon $USERS_THEME_PATH/$INHHRETS/apps/48x48
    find_icon $USERS_THEME_PATH/$INHHRETS/32x32

    # ---- /usr/share/icons/hicolor
    find_icon $HICOLOR_THEME_PATH/scalable/apps
    find_icon $HICOLOR_THEME_PATH/128x128/apps
    find_icon $HICOLOR_THEME_PATH/64x64/apps
    find_icon $HICOLOR_THEME_PATH1/48x48/apps
    find_icon $HICOLOR_THEME_PATH/32x32/apps
    # ---- /usr/share/pixmaps
    find_icon /usr/share/pixmaps

    RESULT=$WAYCONS/application-default-icon.svg

}

_getIcon() {

    if [ -f $WAYCONS/${ICON}.svg ]; then
        RESULT=$WAYCONS/${ICON}.svg
    elif [ -f $WAYCONS/${ICON}.png ]; then
        RESULT=$WAYCONS/${ICON}.png
    else
        _findIconPath
    fi

}


has_launcher=$(hyprctl clients | grep $APP_CLASS)
if [[ ! -z ${has_launcher} ]]; then
    echo ""
    echo ""
else
    _getIcon
    echo "$RESULT"
    echo $APP_CLASS
fi
