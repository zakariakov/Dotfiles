#!/bin/env bash

WAL_DIR=$1

  selected=$(find "$WAL_DIR" -type f \( -iname "*.jpg" -o -iname "*.jpeg" -o -iname "*.png" \) -exec basename {} \; | sort -R | while read rfile; do
    echo -en "$rfile\x00icon\x1f$WAL_DIR/${rfile}\n"
  done | rofi -show -dmenu -theme ~/.config/rofi/themes/config-wallpaper.rasi)
  if [ ! "$selected" ]; then
    echo "No wallpaper selected"
    exit
  fi

  IMAGE_FILE=$WAL_DIR/$selected
  echo "$IMAGE_FILE"
  feh --bg-scale $IMAGE_FILE