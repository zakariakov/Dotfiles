#!/bin/env bach
ARG=$1
VAR_FILE=~/.config/hypr/variables.conf

## ($1)key '=' ($2)value ($3)file
function set_varConfig() {
    sed -i "s/^\(\s*$1\s*=\s*\).*\$/\1$2/" $VAR_FILE
}

hypr_blur() {
    blured=$(cat $VAR_FILE | grep "\$BLUR_ENABLED" | cut -d= -f2)
    if [[ $blured=="true" ]]; then
       set_varConfig "\$BLUR_ENABLED" "false"
    else
       set_varConfig "\$BLUR_ENABLED" "true"
    fi

}

case $ARG in
blur) hypr_blur ;;
02*) globalColorsAccent ;;
esac
