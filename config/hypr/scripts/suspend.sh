#!/bin/bash

## Counter in menute .
## duration default=60 if arg is empty

[ -z "$1" ] && DURATION=60 || DURATION="$1"

counter=0
while (( $counter < $DURATION ))
do

  echo -ne "$counter / $DURATION  M for suspend  System . type Ctrl+c to stop this process \r"
  ((counter++))
  sleep 1m
done

#echo Hello
systemctl suspend  

exit
