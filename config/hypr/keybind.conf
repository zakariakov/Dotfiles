#---------------------------------------------------------------------
#            ░█░█░█▀▀░█░█░█▀▄░▀█▀░█▀█░█▀▄
#            ░█▀▄░█▀▀░░█░░█▀▄░░█░░█░█░█░█
#            ░▀░▀░▀▀▀░░▀░░▀▀░░▀▀▀░▀░▀░▀▀░
#---------------------------------------------------------------------
#bind = ALT, J, togglesplit, # dwindle
#bind = ALT, B, pseudo, # dwindle
#$mode-system = (l) lock | (e) exit | (s) suspend | (r) reboot | (Shift+s) poweroff
#bind =SUPERSHIFT,K, submap, $mode-system
#submap = $mode-system
#bind = , l, exec,  $lock
#bind = , l, submap, reset
#bind = , e, exit
#bind = , s, exec, $lock & systemctl suspend
#bind = , s, submap, reset
#bind = , r, exec, systemctl reboot
#bind = Shift, s, exec, systemctl poweroff

# Keys Help --------------------------------
#
bind =SUPER,H,exec,$KeysHelp

# restart Waybar ---------------------------
#
bind=SUPER,B,exec,killall -SIGUSR1 waybar
bind=SUPERSHIFT,B,exec,$restartbar
#bind=SUPERSHIFT,B,exec,killall -SIGUSR2 waybar
bind=SUPER,N,exec,nwg-dock-hyprland SIGRTMIN+1
bind=SUPERSHIFT,N,exec,$restart-dock
# restart Hyprland -------------------------
#
bind=SUPERSHIFT,R,exec,$restarthypr

# Terminal ---------------------------------
#
bind=SUPER,Return,exec, $term
bind=SUPERALT,Return,exec,wezterm

#bind=SUPERALT,Return,exec,$term  --start-as=fullscreen
bind=SUPER,KP_Enter,exec,$term --class term-floating
#bind=SUPERCTRL,M,exec,urxvt  -fn 'xft:Kata' -fs 14 -e  ncmatrix  -u 10
#bind = SUPER,A,exec,hpr-scratcher toggle sctatchterm

# Apps -------------------------------------
#
bind=SUPERCTRL,F, exec, $files
bind=SUPERCTRL,E, exec, $editor
bind=SUPERCTRL,B, exec, $browser
bind=SUPERCTRL,v, exec, $vimkeys
bind=SUPERSHIFT,W,exec, $wallpaper
bind=SUPERCTRL,W, exec, $selectwal

bind=SUPER,W, exec, $canvaswal
bind=SUPERCTRL,T, exec,$changeTheme
bind=SUPERSHIFT,M,exec, kitty --class "kitty-music" $kittymusic
bind=SUPER,k, exec, $kittyRect

# Rofi -------------------------------------
#
#bindr=SUPER,SUPER_L, exec,nwg-dock-hyprland SIGRTMIN+1
bind=SUPER,Space, exec, $menuDesk
bind=SUPER,R, exec, $menuRun
bind=SUPER,I, exec, $rofiIcons
bind=SUPER,j, exec, $rofiImoji
bind=ALT,Tab, exec, $rofiWindows
bind=SUPERSHIFT,S,exec, $sttings
bind = SUPER,V, exec, $roficliphist

# Misc -------------------------------------
#
# bind=SUPER,     N, exec, nm-connection-editor
bind=SUPER,C, exec, $colorpicker
bind=SUPERSHIFT,X, exec, $wlogout
bind=SUPER,L, exec, hyprlock #$lockscreen

# ZOOM -------------------------------------
#
bind=SUPERCTRL,KP_Add, exec, $zoomin
bind=SUPERCTRL,KP_Subtract, exec, $zoomout
bind=SUPERCTRL,KP_Insert, exec, $zoomfit
bind=SUPERCTRL,mouse_up, exec, $zoomout
bind=SUPERCTRL,mouse_down,exec, $zoomin

# gaps -------------------------------------
#
bind=SUPER,KP_Add, exec, $changegaps u
bind=SUPER,KP_Subtract, exec, $changegaps d
bind=SUPER,KP_Insert, exec, $changegaps

# Tabbed -----------------------------------
#bind= SUPER,g,   togglegroup
#bind= SUPER,tab, changegroupactive

# Some nice mouse binds --------------------
#
bindm=SUPER,mouse:272,movewindow
bindm=SUPER,mouse:273,resizewindow

# Audio volume -----------------------------
#
binde=, XF86AudioRaiseVolume, exec, volumectl -u up
binde=, XF86AudioLowerVolume, exec, volumectl -u down
bind=, XF86AudioMute, exec, volumectl toggle-mute
bind=, XF86AudioMicMute, exec, volumectl -m toggle-mute

# Brightness -------------------------------
#
binde=, XF86MonBrightnessUp, exec, lightctl up
binde=, XF86MonBrightnessDown,exec, lightctl down

# Audio layer ------------------------------
#
bind=, XF86AudioNext, exec, playerctl -i "firefox" next
bind=, XF86AudiPrev, exec, playerctl -i "firefox" previous
#bind=,  XF86Audilay, exec, ~/.config/hypr/scripts/mpris toggle
bind=, XF86AudioPlay, exec, playerctl play-pause
bind=, XF86AudioStop, exec, playerctl -i "firefox" stop

# Camera & Led -----------------------------
bind =,XF86WebCam, exec,sleep 1 && hyprctl dispatch dpms off
bind =,Scroll_Lock,exec,bash -c "pkexec  $SCRIPTS/led-toggle"

# Screenshots ------------------------------grim
#
#bind=,Print,          exec,  $screenshot --now
#bind=CTRL,Print,     exec,  $screenshot --in5
#bind=SUPER,Print,      exec,  $screenshot --win
#bind=SHIFT,Print,     exec,  $screenshot --area

# Screenshots ------------------------------hyprshot
bind=,Print, exec, hyprshot -m output
bind=CTRL,Print, exec, $screenshot --area
bind=SUPER,Print, exec, ~/.config/hypr/scripts/rofi-screenshots.sh

# Plugins ----------------------------------
#
bind=SUPERSHIFT,P, exec, $selectplugin
bind=SUPER,P, exec, $togglebarsplugin

# Hyprland ---------------------------------
#
bind=SUPER,Q, killactive,
bind=SUPER,X, killactive,
bind=CTRLALT,Delete, exit,
bind=SUPER,F, fullscreen,
bind=SUPER,M, fullscreen,1
bind=SUPER,T, togglefloating,
bind=SUPERSHIFT,T, exec,$togglefloatingall,
bind=SUPER,S, exec,$launch-swaync
bind=SUPER,E, exec,$launcheww
# Focus ------------------------------------
#
bind=SUPER,left, movefocus,l
bind=SUPER,right, movefocus,r
bind=SUPER,up, movefocus,u
bind=SUPER,down, movefocus,d

# Move floating ----------------------------
#
binde=SUPERALT,left, moveactive,-20 0
binde=SUPERALT,right, moveactive,10 0
binde=SUPERALT,up, moveactive,0 -20
binde=SUPERALT,down, moveactive,0 20

# Move -------------------------------------
#
bind=SUPERSHIFT,left, movewindow,l
bind=SUPERSHIFT,right,movewindow,r
bind=SUPERSHIFT,up, movewindow,u
bind=SUPERSHIFT,down, movewindow,d

# Resize -----------------------------------
#
binde=SUPERCTRL,left, resizeactive,-20 0
binde=SUPERCTRL,right,resizeactive,20 0
binde=SUPERCTRL,up, resizeactive,0 -20
binde=SUPERCTRL,down, resizeactive,0 20

# Workspaces -------------------------------
#
bind=SUPER,ampersand, workspace,1
bind=SUPER,eacute, workspace,2
bind=SUPER,quotedbl, workspace,3
bind=SUPER,apostrophe, workspace,4
bind=SUPER,parenleft, workspace,5
bind=SUPER,minus, workspace,6
bind=SUPER,egrave, workspace,7
bind=SUPER,underscore,workspace,8
bind=SUPER,ccedilla, workspace,9

# Send to Workspaces -----------------------
#
bind=SUPERSHIFT,ampersand, movetoworkspace,1
bind=SUPERSHIFT,eacute, movetoworkspace,2
bind=SUPERSHIFT,quotedbl, movetoworkspace,3
bind=SUPERSHIFT,apostrophe, movetoworkspace,4
bind=SUPERSHIFT,parenleft, movetoworkspace,5
bind=SUPERSHIFT,minus, movetoworkspace,6
bind=SUPERSHIFT,egrave, movetoworkspace,7
bind=SUPERSHIFT,underscore,movetoworkspace,8
bind=SUPERSHIFT,ccedilla, movetoworkspace,9

# Switch workspace by mouse wheel ----------
#
bind=CTRLALT,right, workspace,e-1
bind=CTRLALT,left, workspace,e+1

# Switch workspace by mouse wheel ----------
#
bind=SUPER,mouse_down, workspace,e+1
bind=SUPER,mouse_up, workspace,e-1

# layoutmsg --------------------------------
#
#bind=SUPERALT,D, togglesplit, # dwindle
bind=SUPERALT,H, layoutmsg,orientationright
bind=SUPERALT,J, layoutmsg,orientationleft
bind=SUPERALT,K, layoutmsg,orientationt
bind=SUPERALT,L, layoutmsg,orientationbottom

# Scratchpad ------------------------------------
#
bind=SUPERSHIFT,A, movetoworkspace, special
bind=SUPER,A,togglespecialworkspace,
