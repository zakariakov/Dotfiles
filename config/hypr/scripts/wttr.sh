#!/usr/bin/env bash


#------------------------------------------------------

DATE=$(date +%m_%d_%y)
WTTR_CACHE=~/.cache/wttr_${DATE}.json

HOUR=$(date +%H)

case $HOUR in
00 | 01 | 02) INDEX='0' ;;
03 | 04 | 05) INDEX='1' ;;
06 | 07 | 08) INDEX='2' ;;
09 | 10 | 11) INDEX='3' ;;
12 | 13 | 14) INDEX='4' ;;
15 | 16 | 17) INDEX='5' ;;
18 | 19 | 20) INDEX='6' ;;
21 | 22 | 23) INDEX='7' ;;
esac

WEATHER_CODES() {
    case $CODE in

    113) W_ICON='🌞' ;;
    116) W_ICON='⛅️' ;;
    119) W_ICON='☁️' ;;
    122) W_ICON='☁️' ;;
    143) W_ICON='🌫' ;;
    176) W_ICON='🌦' ;;
    179) W_ICON='🌧' ;;
    182) W_ICON='🌧' ;;
    185) W_ICON='🌧' ;;
    200) W_ICON='⛈' ;;
    227) W_ICON='🌨' ;;
    230) W_ICON='❄️' ;;
    248) W_ICON='🌫' ;;
    260) W_ICON='🌫' ;;
    263) W_ICON='🌦' ;;
    266) W_ICON='🌦' ;;
    281) W_ICON='🌧' ;;
    284) W_ICON='🌧' ;;
    293) W_ICON='🌦' ;;
    296) W_ICON='🌦' ;;
    299) W_ICON='🌧' ;;
    302) W_ICON='🌧' ;;
    305) W_ICON='🌧' ;;
    308) W_ICON='🌧' ;;
    311) W_ICON='🌧' ;;
    314) W_ICON='🌧' ;;
    317) W_ICON='🌧' ;;
    320) W_ICON='🌨' ;;
    323) W_ICON='🌨' ;;
    326) W_ICON='🌨' ;;
    329) W_ICON='❄️' ;;
    332) W_ICON='❄️' ;;
    335) W_ICON='❄️' ;;
    338) W_ICON='❄️' ;;
    350) W_ICON='🌧' ;;
    353) W_ICON='🌦' ;;
    356) W_ICON='🌧' ;;
    359) W_ICON='🌧' ;;
    362) W_ICON='🌧' ;;
    365) W_ICON='🌧' ;;
    368) W_ICON='🌨' ;;
    371) W_ICON='❄️' ;;
    374) W_ICON='🌧' ;;
    377) W_ICON='🌧' ;;
    386) W_ICON='⛈' ;;
    389) W_ICON='🌩' ;;
    392) W_ICON='⛈' ;;
    395) W_ICON='❄️' ;;
    esac
}

function json() {
    jq --unbuffered --null-input --compact-output \
        --arg text "$1" \
        --arg tooltip "$2" \
        '{"text": $text, "tooltip": $tooltip}'
}

if [[ $1 == r ]]; then

    wget https://wttr.in/ALG\?format\=j1\&lang\=ar -O $WTTR_CACHE

elif [[  ! -f $WTTR_CACHE ]]; then
    rm ~/.cache/wttr_*
    wget https://wttr.in/ALG\?format\=j1\&lang\=ar -O $WTTR_CACHE

fi

declare -i SIZE=$(ls -s $WTTR_CACHE  | awk '{print $1}')

if (( $SIZE <= 10 )); then
     rm $WTTR_CACHE
     wget https://wttr.in/ALG\?format\=j1\&lang\=ar -O $WTTR_CACHE
fi

WeatherDesc=$(cat $WTTR_CACHE | jq -r ".weather[0].hourly[$INDEX].lang_ar[0].value")
CODE=$(cat $WTTR_CACHE | jq -r ".weather[0].hourly[$INDEX].weatherCode")
FeelsLikeC=$(cat $WTTR_CACHE | jq -r ".weather[0].hourly[$INDEX].FeelsLikeC")
Humidity=$(cat $WTTR_CACHE | jq -r ".weather[0].hourly[$INDEX].humidity")
WindspeedKmph=$(cat $WTTR_CACHE | jq -r ".weather[0].hourly[$INDEX].windspeedKmph")
Time=$(cat $WTTR_CACHE | jq -r ".weather[0].hourly[$INDEX].time")

WEATHER_CODES $CODE
#echo $HOUR
#echo $Time
#echo $INDEX

Text="<span size='large'>$W_ICON</span> ${FeelsLikeC}°C"
ToolTip="$W_ICON ${FeelsLikeC}°C  "
ToolTip+="${WeatherDesc}  "
ToolTip+="<b>Humidity:</b> ${Humidity}%  "
ToolTip+="<b>Windspeed:</b> ${WindspeedKmph}km/h "

json "$Text" "$ToolTip"
