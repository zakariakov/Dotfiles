local wezterm = require("wezterm")
local act = wezterm.action
-- local launch_menu = {}
-- local default_shell = "/bin/zsh"
local padding = {
	left = "0",
	right = "0",
	top = "0",
	bottom = "0",
}

return {
	bidi_enabled = false,
	bidi_direction = "AutoLeftToRight",
	color_scheme =  "Gruvbox Dark",
    	color_scheme_dirs = { '/home/yahia/.config/wezterm/colors/' },
	tab_bar_at_bottom=true,

  font = wezterm.font 'Kata',
  -- You can specify some parameters to influence the font selection;
  -- for example, this selects a Bold, Italic font variant.
 -- font = wezterm.font('Hack Nerd Fonnt', { weight = 'Bold', italic = true }),

	
	initial_cols = 128,
	initial_rows = 32,
	use_dead_keys = false,
	window_padding = padding,
	window_decorations = "RESIZE",
	hide_tab_bar_if_only_one_tab = true,
	selection_word_boundary = " \t\n{}[]()\"'`,;:@",
	-- disable_default_key_bindings = true,
	line_height = 1,
    cell_width = 1.0,
	font_size = 13,
	window_background_opacity = 1,
	bold_brightens_ansi_colors = false,
	-- swap_backspace_and_delete = false,
	-- term = "wezterm",
	-- freetype_load_target = "Light",
    
}
