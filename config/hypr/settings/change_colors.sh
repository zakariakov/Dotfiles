#!/bin/env bash
ARG=$1
HPRVAR_FILE=~/.config/hypr/variables.conf
EWW_FILE=~/.config/hypr/hypr-side/src/hyper.scss
WAYBAR_FILE=~/.config/waybar/colors/colors.css
ROFI_FILE=~/.config/rofi/themes/colors/colors.rasi
GTK3_CSS=~/.config/gtk-3.0/gtk.css
GTK4_CSS=~/.config/gtk-4.0/gtk.css
QT5_FILE=~/.config/qt5ct/colors/wal.conf
WEZTERM_FILE=~/.config/wezterm/colors/wal.toml
WEZTERM_DIR=~/.config/wezterm
BLACKBOX_FILE=~/.local/share/blackbox/schemes/wal.json
KITTYFILE=~/.config/kitty/wal.conf
ZATHURAFILE=~/.config/zathura/zathurarc
ZATHURADIR=~/.config/zathura

## ----------------------------------------------------------------------
xrdb load ~/.cache/wal/colors.Xresources
xrdb merge ~/.cache/wal/colors.Xresources

## ($1)key '=' ($2)value ($3)file
function set_configH() {
    sed -i "s/^\(\s*$1\s*=\s*\).*\$/\1$2/" "$3"
}
## ($1)key ':' ($2)value ($3)file
function set_configX() {
    sed -i "s/^\(\s*$1\s*:\s*\).*\$/\1$2/" "$3"
}
## ($1)key '#' ($2)value ($3)file
function set_colorgkcss() {
    sed -i "s/^\($1\s*#\s*\).*\$/\1$2/" "$3"
}
## darcker color ---------------------
function darker_channel() {

    local value="0x$1"
    local light_delta="0x$2"
    local -i result

    result=$((value + light_delta))

    ((result < 0)) && result=0
    ((result > 255)) && result=255

    echo "$result"
}
## Convert darcker color ---------------------
function darker() {
    local hexinput="$1"
    local light_delta=${2-10}

    r=$(darker_channel "${hexinput:0:2}" "$light_delta")
    g=$(darker_channel "${hexinput:2:2}" "$light_delta")
    b=$(darker_channel "${hexinput:4:2}" "$light_delta")

    printf '%02x%02x%02x\n' "$r" "$g" "$b"
}

# ----------------------------------------------------------------------
get_wal_color() {
    ACC_COL=$(xrdb -get color5)
    ACC_COL=$(echo $ACC_COL | tr -d '#')

    BGR_COL=$(xrdb -get background)
    BGR_COL=$(echo $BGR_COL | tr -d '#')

    FGR_COL=$(xrdb -get foreground)
    FGR_COL=$(echo $FGR_COL | tr -d '#')

    RED_COL=$(xrdb -get color1)
    RED_COL=$(echo $RED_COL | tr -d '#')

    GRN_COL=$(xrdb -get color2)
    GRN_COL=$(echo $GRN_COL | tr -d '#')

    YLW_COL=$(xrdb -get color3)
    YLW_COL=$(echo $YLW_COL | tr -d '#')

    BG=#$BGR_COL
    FG=#$FGR_COL
    COL0=$(xrdb -get color0)
    COL1=#$RED_COL
    COL2=#$GRN_COL
    COL3=#$YLW_COL
    COL4=#$ACC_COL
    COL5=$(xrdb -get color5)
    COL6=$(xrdb -get color6)
    COL7=$(xrdb -get color7)
    COL8=$(xrdb -get color8)
    COL9=$(xrdb -get color9)
    COL10=$(xrdb -get color10)
    COL11=$(xrdb -get color11)
    COL12=$(xrdb -get color12)
    COL13=$(xrdb -get color13)
    COL14=$(xrdb -get color14)
    COL15=$(xrdb -get color15)

    # ---------------------------------------------------------------------
    BGR_lIGHT="$(darker "$BGR_COL" 12)"
    BGR_SIDE="$(darker "$BGR_COL" 6)"
    fGR_lIGHT="$(darker "$FGR_COL" -12)"

}
# ----------------------------------------------------------------------
change_hypr_colors() {

    set_configH "\$BORDER_MULTI_COLOR" "0" ${HPRVAR_FILE}
    set_configH "\$COL_ACTIVE_BORDER" "rgb($ACC_COL)" ${HPRVAR_FILE}
    set_configH "\$MULTI_COLOR" "rgb($ACC_COL)" ${HPRVAR_FILE}

    set_configH "\$ACCENT_COL" "$ACC_COL" ${HPRVAR_FILE}

    set_configH "\$COL_INACTIVE_BORDER" "rgb($BGR_COL)" ${HPRVAR_FILE}
    set_configH "\$COL_SHADOW" "rgba(${ACC_COL}bb)" ${HPRVAR_FILE}
    #- for plugin bahyprbars button and title
    set_configH "\$COL_BG" "rgb(${BGR_COL})" ${HPRVAR_FILE}
    set_configH "\$COL_FG" "rgb(${FGR_COL})" ${HPRVAR_FILE}
    set_configH "\$COL_RED" "rgb(${RED_COL})" ${HPRVAR_FILE}
    set_configH "\$COL_GREEN" "rgb(${GRN_COL})" ${HPRVAR_FILE}
    set_configH "\$COL_YELLOW" "rgb(${YLW_COL})" ${HPRVAR_FILE}
}
# ----------------------------------------------------------------------
change_rofi_colors() {

    set_configX "background" "#${BGR_COL}9a;" ${ROFI_FILE}
    set_configX "background-opac" "#${BGR_COL};" ${ROFI_FILE}
    set_configX "background-light" "#${BGR_lIGHT};" ${ROFI_FILE}
    set_configX "foreground" "#${FGR_COL};" ${ROFI_FILE}
    set_configX "background-focus" "#${ACC_COL};" ${ROFI_FILE}

}
# ----------------------------------------------------------------------
change_eww_colors() {

    set_colorgkcss "\$black:" "${BGR_COL};" ${EWW_FILE}
    set_colorgkcss "\$blackAlt:" "${BGR_lIGHT};" ${EWW_FILE}
    set_colorgkcss "\$white:" "${FGR_COL};" ${EWW_FILE}
    set_colorgkcss "\$accent:" "${ACC_COL};" ${EWW_FILE}
}
# ----------------------------------------------------------------------
change_waybar_colors() {

    set_colorgkcss "@define-color background" "${BGR_COL};" ${WAYBAR_FILE}
    set_colorgkcss "@define-color foreground" "${FGR_COL};" ${WAYBAR_FILE}
    set_colorgkcss "@define-color accent" "${ACC_COL};" ${WAYBAR_FILE}
}
# ----------------------------------------------------------------------
change_gtk3_colors() {

    set_colorgkcss "@define-color accent_color" "${ACC_COL};" ${GTK3_CSS}
    set_colorgkcss "@define-color accent_bg_color" "${ACC_COL};" ${GTK3_CSS}

    set_colorgkcss "@define-color window_bg_color" "${BGR_COL};" ${GTK3_CSS}
    set_colorgkcss "@define-color window_fg_color" "${FGR_COL};" ${GTK3_CSS}

    set_colorgkcss "@define-color view_bg_color" "${BGR_lIGHT};" ${GTK3_CSS}
    set_colorgkcss "@define-color view_fg_color" "${FGR_COL};" ${GTK3_CSS}

    set_colorgkcss "@define-color headerbar_bg_color" "${BGR_COL};" ${GTK3_CSS}
    set_colorgkcss "@define-color headerbar_fg_color" "${FGR_COL};" ${GTK3_CSS}

    set_colorgkcss "@define-color dialog_bg_color" "${BGR_COL};" ${GTK3_CSS}
    set_colorgkcss "@define-color dialog_fg_color" "${FGR_COL};" ${GTK3_CSS}

    set_colorgkcss "@define-color popover_bg_color" "${BGR_lIGHT};" ${GTK3_CSS}
    set_colorgkcss "@define-color popover_fg_color" "${FGR_COL};" ${GTK3_CSS}

}
# ----------------------------------------------------------------------
change_gtk4_colors() {
    set_colorgkcss "@define-color accent_color" "${ACC_COL};" ${GTK4_CSS}
    set_colorgkcss "@define-color accent_bg_color" "${ACC_COL};" ${GTK4_CSS}

    set_colorgkcss "@define-color window_bg_color" "${BGR_COL};" ${GTK4_CSS}
    set_colorgkcss "@define-color window_fg_color" "${FGR_COL};" ${GTK4_CSS}

    set_colorgkcss "@define-color view_bg_color" "${BGR_lIGHT};" ${GTK4_CSS}
    set_colorgkcss "@define-color view_fg_color" "${FGR_COL};" ${GTK4_CSS}

    set_colorgkcss "@define-color headerbar_bg_color" "${BGR_COL};" ${GTK4_CSS}
    set_colorgkcss "@define-color headerbar_fg_color" "${FGR_COL};" ${GTK4_CSS}

    set_colorgkcss "@define-color dialog_bg_color" "${BGR_COL};" ${GTK4_CSS}
    set_colorgkcss "@define-color dialog_fg_color" "${FGR_COL};" ${GTK4_CSS}

    set_colorgkcss "@define-color popover_bg_color" "${BGR_lIGHT};" ${GTK4_CSS}
    set_colorgkcss "@define-color popover_fg_color" "${FGR_COL};" ${GTK4_CSS}

    set_colorgkcss "@define-color sidebar_bg_color" "${BGR_SIDE};" ${GTK4_CSS}
    set_colorgkcss "@define-color sidebar_fg_color" "${FGR_COL};" ${GTK4_CSS}

    set_colorgkcss "@define-color sidebar_backdrop_color" "${BGR_lIGHT};" ${GTK4_CSS}

    set_colorgkcss "@define-color secondary_sidebar_bg_color" "${BGR_lIGHT};" ${GTK4_CSS}
    set_colorgkcss "@define-color secondary_sidebar_fg_color" "${FGR_COL};" ${GTK4_CSS}
    set_colorgkcss "@define-color secondary_sidebar_backdrop_color" "${BGR_lIGHT};" ${GTK4_CSS}
}
# ----------------------------------------------------------------------
change_qt5_colors() {
    # 1-WIN_txt      2-BTN_BG       3-bright         4-less            5-dark
    # 6-dark_less    7-TXT_normal   8-TXT_bright     9-BTN_TXT        10-WIN_normal
    #11-window       12-shadow      13-highlight     14-higl-txt       15-link
    #16-link-vist    17-win-replac  18-defaulte      19-bg-tooltip     20-tooltip-txt   21-placeholder_txt
    echo "[ColorScheme]" >${QT5_FILE}
    echo "active_colors= \
  #ff${FGR_COL},   #ff${BGR_COL}, #ff${BGR_lIGHT}, #dd${BGR_lIGHT}, #ff${BGR_COL},\
  #ff${BGR_lIGHT}, #ff${FGR_COL}, $COL7,           #ff${FGR_COL},   #ff${BGR_lIGHT}, \
  #ff${BGR_COL},   #ff${FGR_COL}, #ff${ACC_COL},   #ff${BGR_COL},   #ff0986d3, \
  #ff9619a7,       #ff5c5b5a,     #ffffffff,      #ff353535,       #ffffffff,      #80ffffff" \
        >>${QT5_FILE}

    echo "disabled_colors= \
  #40${FGR_COL},   #80${BGR_COL}, #40${BGR_lIGHT}, #40${BGR_lIGHT},  #40${BGR_COL},\
  #40${BGR_lIGHT}, #40${FGR_COL}, #40${FGR_COL},   #40${FGR_COL},    #ff${BGR_lIGHT}, \
  #ff${BGR_COL},   #40${FGR_COL}, #40${ACC_COL},   #40${FGR_COL},    #ff0986d3, \
  #ff9619a7,       #ff5c5b5a,     #ffffffff,       #ff353535,        #ffffffff,      #80ffffff" \
        >>${QT5_FILE}

    echo "inactive_colors=  \
  #40${FGR_COL},   #80${BGR_COL}, #40${BGR_lIGHT},  #40${BGR_lIGHT},  #40${BGR_COL},\
  #40${BGR_lIGHT}, #40${FGR_COL}, #40${FGR_COL},    #40${FGR_COL},    #40${BGR_lIGHT}, \
  #ff${BGR_COL},   #40${FGR_COL}, #40${ACC_COL},    #40${FGR_COL},    #ff0986d3, \
  #ff9619a7,       #ff5c5b5a,     #ffffffff,        #ff353535,         #ffffffff,      #80ffffff" \
        >>${QT5_FILE}
}

# ----------------------------------------------------------------------
change_blackbox_colors() {
    echo "{" >$BLACKBOX_FILE
    echo "  \"name\": \"wal Dark\"," >>$BLACKBOX_FILE
    echo "  \"comment\": \"created by blackbox Colour Scheme\"," >>$BLACKBOX_FILE
    echo "  \"use-theme-colors\": false," >>$BLACKBOX_FILE
    echo "  \"foreground-color\": \"$FG\"," >>$BLACKBOX_FILE
    echo "  \"background-color\": \"$BG\"," >>$BLACKBOX_FILE
    echo "  \"palette\": [" >>$BLACKBOX_FILE
    echo "    \"$COL0\"," >>$BLACKBOX_FILE
    echo "    \"$COL1\"," >>$BLACKBOX_FILE
    echo "    \"$COL2\"," >>$BLACKBOX_FILE
    echo "    \"$COL3\"," >>$BLACKBOX_FILE
    echo "    \"$COL4\"," >>$BLACKBOX_FILE
    echo "    \"$COL5\"," >>$BLACKBOX_FILE
    echo "    \"$COL6\"," >>$BLACKBOX_FILE
    echo "    \"$COL7\"," >>$BLACKBOX_FILE
    echo "    \"$COL8\"," >>$BLACKBOX_FILE
    echo "    \"$COL9\"," >>$BLACKBOX_FILE
    echo "    \"$COL10\"," >>$BLACKBOX_FILE
    echo "    \"$COL11\"," >>$BLACKBOX_FILE
    echo "    \"$COL12\"," >>$BLACKBOX_FILE
    echo "    \"$COL13\"," >>$BLACKBOX_FILE
    echo "    \"$COL14\"," >>$BLACKBOX_FILE
    echo "    \"$COL15\"" >>$BLACKBOX_FILE
    echo "  ]" >>$BLACKBOX_FILE
    echo "}" >>$BLACKBOX_FILE
}

# ----------------------------------------------------------------------
change_accent_color() {
    #  ACC_COL=$ARG
    ACC_COL=$(echo $ARG | tr -d '#')

    OLD_AAC_COL=$(cat $HPRVAR_FILE | grep "\$ACCENT_COL" | cut -d= -f2)

    OLD_AAC_COL=$(echo $OLD_AAC_COL | tr -d '#')

    if ! [[ $ACC_COL =~ ^[0-9A-Fa-f]{6}$ ]]; then
        echo "color no valid  $ACC_COL"
        notify-send "Colors Accent" "color no valid  #$ACC_COL"
        exit
    fi
    #------------------------------Qt5t
    echo "old color: $OLD_AAC_COL"
    echo "new color: $ACC_COL"

    cp -f ${QT5_FILE} /$HOME/.cache/qt5wal
    sed "s/$OLD_AAC_COL/$ACC_COL/g" "/$HOME/.cache/qt5wal" >"${QT5_FILE}"
    #-----------------------------FFB400
    set_configH "\$COL_ACTIVE_BORDER" "rgb($ACC_COL)" ${HPRVAR_FILE}
    set_configH "\$MULTI_COLOR" "rgb($ACC_COL)" ${HPRVAR_FILE}
    set_configH "\$COL_SHADOW" "rgba(${ACC_COL}bb)" ${HPRVAR_FILE}
    set_configH "\$ACCENT_COL" "$ACC_COL" ${HPRVAR_FILE}
    set_configX "background-focus" "#${ACC_COL};" ${ROFI_FILE}
    set_colorgkcss "\$accent:" "${ACC_COL};" ${EWW_FILE}
    set_colorgkcss "@define-color accent" "${ACC_COL};" ${WAYBAR_FILE}
    set_colorgkcss "@define-color accent_color" "${ACC_COL};" ${GTK3_CSS}
    set_colorgkcss "@define-color accent_bg_color" "${ACC_COL};" ${GTK3_CSS}
    set_colorgkcss "@define-color accent_color" "${ACC_COL};" ${GTK4_CSS}
    set_colorgkcss "@define-color accent_bg_color" "${ACC_COL};" ${GTK4_CSS}
    # notify-send "Colors Accent" "#$ACC_COL"
    # hyprctl reload
    # exit
}

# ----------------------------------------------------------------------
if [[ "$ARG" == "wal" ]]; then

    if [ ! -f ~/.cache/wal/colors.Xresources ]; then
        notify-send "Colors Accent" "file no found ~/.cache/wal/colors.Xresources"
        exit
    fi

    hyprctl notify 1 3000 "rgb(FFB400)" "fontsize:25 applly colorsheme from wal "
    get_wal_color

    if [ -f $HPRVAR_FILE ]; then
        echo ">>  $HPRVAR_FILE "
        change_hypr_colors
    fi
    sleep 0.1
    if [ -f $ROFI_FILE ]; then
        echo ">>  $ROFI_FILE "
        change_rofi_colors
    fi
    sleep 0.1
    if [ -f $EWW_FILE ]; then
        echo ">>  $EWW_FILE "
        change_eww_colors
    fi
    sleep 0.1
    if [ -f $WAYBAR_FILE ]; then
        echo ">>  $WAYBAR_FILE "
        change_waybar_colors
    fi
    sleep 0.1
    if [ -f $GTK3_CSS ]; then
        echo ">>  $GTK3_CSS "
        change_gtk3_colors
    fi
    sleep 0.1
    if [ -f $GTK4_CSS ]; then
        change_gtk4_colors
        echo ">>  $GTK4_CSS "
    fi
    sleep 0.1
    if [ -f $QT5_FILE ]; then
        echo ">>  $QT5_FILE "
        change_qt5_colors
    fi
    sleep 0.1
    if [[ "$ARG" == "wal" ]]; then
        cp -f ~/.cache/wal/colors-kitty.conf $KITTYFILE
    fi
    sleep 0.1
    if [ -f $WEZTERM_DIR/create_wal_colors.sh ]; then
        echo ">>  $WEZTERM_FILE "
        mv -f $WEZTERM_FILE ${WEZTERM_FILE}.back
        $WEZTERM_DIR/create_wal_colors.sh >>"$WEZTERM_FILE"
        # change_wezterm_colors
    fi
    sleep 0.1
    if [ -f $BLACKBOX_FILE ]; then
        echo ">>  $BLACKBOX_FILE "
        change_blackbox_colors
    fi

    sleep 0.1
    ## git clone https://github.com/GideonWolfe/Zathura-Pywal.git
    if [ -f $ZATHURADIR/wal_genzathurarc ]; then
        echo ">>  $ZATHURAFILE "
        mv -f $ZATHURAFILE ${ZATHURAFILE}.back
        $ZATHURADIR/wal_genzathurarc >>"$ZATHURAFILE"
    fi

else

    hyprctl notify 1 1500 "rgb(FFB400)" "fontsize:25 applly color accent $ARG"
    change_accent_color
    exit

fi

#--QT5 ------------------------------------

sleep 0.1
#nohup ~/.config/hypr/scripts/statusbar r >/tmp/waybar.log &
swaync-client -rs
#notify-send "Colors Accent" "$ACC_COL"
hyprctl reload

## yay -S  wal-telegram-git
wal-telegram -g -w
sleep 2
## git clone https://github.com/GideonWolfe/Zathura-Pywal.git
## cd Zathura-Pywal
## ./install.sh

exit

# -----------------------------------------------------

#if [[ "$ARG" == "wal" ]]; then
#    cp -f ~/.cache/wal/colors-kitty.conf $KITTYFILE
#else
#    echo "foreground $FG" >$KITTYFILE
#    echo "background $BG" >>$KITTYFILE
#    echo "cursor     $FG" >>$KITTYFILE
#    echo "color0     $COL0" >>$KITTYFILE
#    echo "color1     $COL1" >>$KITTYFILE
#    echo "color2     $COL2" >>$KITTYFILE
#    echo "color3     $COL3" >>$KITTYFILE
#    echo "color4     $COL4" >>$KITTYFILE
#    echo "color5     $COL5" >>$KITTYFILE
#    echo "color6     $COL6" >>$KITTYFILE
#    echo "color7     $COL7" >>$KITTYFILE
#    echo "color8     $COL8" >>$KITTYFILE
#    echo "color9     $COL9" >>$KITTYFILE
#    echo "color10    $COL10" >>$KITTYFILE
#    echo "color11    $COL11" >>$KITTYFILE
#    echo "color12    $COL12" >>$KITTYFILE
#    echo "color13    $COL13" >>$KITTYFILE
#    echo "color14    $COL14" >>$KITTYFILE
#    echo "color15    $COL15" >>$KITTYFILE
#fi

# ----------------------------------------------------------------------
#change_zathura_colors() {
#    echo "set default-fg                 \"$FG\" " >$ZATHURAFILE
#    echo "set default-bg 			           \"$BG\" " >>$ZATHURAFILE
#    echo "set completion-bg		           \"#$BGR_lIGHT\"" >>$ZATHURAFILE
#    echo "set completion-highlight-bg	   \"$COL5\" " >>$ZATHURAFILE
#    echo "set completion-highlight-fg	   \"$BG\" " >>$ZATHURAFILE
#    echo "set completion-group-bg		     \"$BG\" " >>$ZATHURAFILE
#    echo "set completion-group-fg		     \"$FG\" " >>$ZATHURAFILE
#    echo "set statusbar-fg		           \"$FG\" " >>$ZATHURAFILE
#    echo "set statusbar-bg		           \"#$BGR_lIGHT\" " >>$ZATHURAFILE
#    echo "set notification-bg		         \"$BG\" " >>$ZATHURAFILE
#    echo "set notification-fg		         \"$FG\" " >>$ZATHURAFILE
#    echo "set notification-error-bg	     \"#464545\"" >>$ZATHURAFILE
#    echo "set notification-error-fg	     \"#f28fad\"" >>$ZATHURAFILE
#    echo "set notification-warning-bg	   \"#464545\"" >>$ZATHURAFILE
#    echo "set notification-warning-fg	   \"#fae3b0\"" >>$ZATHURAFILE
#    echo "set inputbar-fg			            \"$FG\" " >>$ZATHURAFILE
#    echo "set inputbar-bg 		            \"#$BGR_lIGHT\" " >>$ZATHURAFILE
#    echo "set recolor-lightcolor		      \"$BG\" " >>$ZATHURAFILE
#    echo "set recolor-darkcolor		        \"$FG\" " >>$ZATHURAFILE
#    echo "set index-fg			              \"$COL15\" " >>$ZATHURAFILE
#    echo "set index-bg			              \"$BG\" " >>$ZATHURAFILE
#    echo "set index-active-fg		          \"$COL15\" " >>$ZATHURAFILE
#    echo "set index-active-bg		          \"$COL8\" " >>$ZATHURAFILE
#    echo "set render-loading-bg		        \"$BG\" " >>$ZATHURAFILE
#    echo "set render-loading-fg		        \"$FG\" " >>$ZATHURAFILE
#    echo "set highlight-color		          \"#$BGR_lIGHT\" " >>$ZATHURAFILE
#    echo "set highlight-fg                \"$FG\" " >>$ZATHURAFILE
#    echo "set highlight-active-color	    \"#$BGR_lIGHT\" " >>$ZATHURAFILE
#
#}

# ----------------------------------------------------------------------
#change_wezterm_colors() {
#    echo "[colors]" >$WEZTERM_FILE
#    echo "ansi = [" >>$WEZTERM_FILE
#    echo "	'$COL0'," >>$WEZTERM_FILE
#    echo "	'$COL1'," >>$WEZTERM_FILE
#    echo "	'$COL2'," >>$WEZTERM_FILE
#    echo "	'$COL3'," >>$WEZTERM_FILE
#    echo "	'$COL4'," >>$WEZTERM_FILE
#    echo "	'$COL5'," >>$WEZTERM_FILE
#    echo "	'$COL6'," >>$WEZTERM_FILE
#    echo "	'$COL7'," >>$WEZTERM_FILE
#    echo "]" >>$WEZTERM_FILE
#    echo "background = '$BG'" >>$WEZTERM_FILE
#    echo "brights = [" >>$WEZTERM_FILE
#    echo "	'$COL8'," >>$WEZTERM_FILE
#    echo "	'$COL9'," >>$WEZTERM_FILE
#    echo "	'$COL10'," >>$WEZTERM_FILE
#    echo "	'$COL11'," >>$WEZTERM_FILE
#    echo "	'$COL12'," >>$WEZTERM_FILE
#    echo "	'$COL13'," >>$WEZTERM_FILE
#    echo "	'$COL14'," >>$WEZTERM_FILE
#    echo "	'$COL15'," >>$WEZTERM_FILE
#    echo "]" >>$WEZTERM_FILE
#    echo "cursor_bg = '$FG'" >>$WEZTERM_FILE
#    echo "cursor_border = '$FG'" >>$WEZTERM_FILE
#    echo "cursor_fg = '$BG'" >>$WEZTERM_FILE
#    echo "foreground = '$FG'" >>$WEZTERM_FILE
#    echo "[metadata]" >>$WEZTERM_FILE
#    echo "name = 'wal'" >>$WEZTERM_FILE
#
#}
