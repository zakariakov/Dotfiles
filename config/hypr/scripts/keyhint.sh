#!/bin/sh

ANC=(
    "<span foreground='#b2ffc1'><b> +</b>     </span>            (focus)   focus next/prev widow|
<span foreground='#b2ffc1'><b>+Shift+     </b></span>        (move)   move widow|
<span foreground='#b2ffc1'><b>+Ctrl+      </b></span>        (resize)   resize widow|
<span foreground='#b2ffc1'><b>+Alt+       </b></span>        (move) floatig   move active floating|
<span foreground='#b2ffc1'><b>++/-         </b></span>        (gaps)   gaps plus or minus|
<span foreground='#b2ffc1'><b>+X           </b></span>        (kill)   close focused app|
<span foreground='#b2ffc1'><b>+Enter/KP-enter</b></span>     (Terminal)   terminal tiling/floating|
<span foreground='#b2ffc1'><b>+R           </b></span>       (rofi)   Run Menu|
<span foreground='#b2ffc1'><b>+I           </b></span>       (rofi)   awesome char|
<span foreground='#b2ffc1'><b>+J           </b></span>       (rofi)   emoji|
<span foreground='#b2ffc1'><b>+Ctrl+V      </b></span>       (rofi)   vim key help|
<span foreground='#b2ffc1'><b>+Space       </b></span>       (rofi)   Applications Menu|
<span foreground='#b2ffc1'><b>+Print       </b></span>       (rofi)   screenshot utils|
<span foreground='#b2ffc1'><b>Print         </b></span>       (grim)   screenshot|
<span foreground='#b2ffc1'><b>+Shift+X    </b></span>       (wlogout)   power menu|
<span foreground='#b2ffc1'><b>+Shift+W    </b></span>       (swwww)      Change wallpaper|
<span foreground='#b2ffc1'><b>+Ctrl+W     </b></span>       (swwww)   Select wallpaper|
<span foreground='#b2ffc1'><b>+W          </b></span>       (swwww)   canvas wallpaper|
<span foreground='#b2ffc1'><b>+F          </b></span>       (Fullscreen)   Toggles to full screen|
<span foreground='#b2ffc1'><b>+M          </b></span>       (Fake) fullscreen   Behave full screen without full screen|
<span foreground='#b2ffc1'><b>+T          </b></span>       (Float)   Toggle windows to float|
<span foreground='#b2ffc1'><b>+P          </b></span>       (plugin)   Toggle window title buttons plugins|
<span foreground='#b2ffc1'><b>+Shift+P    </b></span>       (plugins)   Toggle selected plugin|
<span foreground='#b2ffc1'><b>+Shift+B    </b></span>       (waybar)   Toggle hide/show waybar|
<span foreground='#b2ffc1'><b>+Shift+B    </b></span>       (nwg-dock )  Toggle hide/show dock bar|
<span foreground='#b2ffc1'><b>+Shift+S    </b></span>      (Settings)  Display Settings|
<span foreground='#b2ffc1'><b>+S          </b></span>       (sawync)   Toggle hide/show notify center|
<span foreground='#b2ffc1'><b>+Shift+A    </b></span>       (Scratchpad)   move to special workspace|
<span foreground='#b2ffc1'><b>+A          </b></span>       (Scratchpad)   Scratchpad toggle|
<span foreground='#b2ffc1'><b>+Ctrl++/-   </b></span>       (Zoom)   screen ZoomIn/ZoomOut|
<span foreground='#b2ffc1'><b>+Ctrl+0     </b></span>       (ZoomFit)   screen Zoom Orogenal fit|")
echo $ANC | rofi -sep "|" -markup-rows -dmenu -i -p 'hyprland key: ' "" -columns 1
