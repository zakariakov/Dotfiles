#!/bin/env bash

#if [[ $1 == "toggle" ]]; then

if [ ! -z $(hyprshade current) ]; then
    echo 'btn_Off'  > /tmp/redshift.txt
else
    echo 'btn_On'  > /tmp/redshift.txt
fi
sleep 0.5
 hyprshade toggle

# fi
exit 0
