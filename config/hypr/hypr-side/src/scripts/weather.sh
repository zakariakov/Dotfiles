#!/bin/bash

# Closeboc73
# This script is to get weather data from openweathermap.com in the form of a json file
# so that conky will still display the weather when offline even though it doesn't up to date

FileName=~/.cache/weather.json
Date_Modif=$(date -r $FileName +"%H%d")
Date_Current=$(date '+ %H%d')

if [[ "$Date_Current" != "$Date_Modif" ]]; then
     #   echo $Date_Modif "==" $Date_Current
        # you can use this or replace with yours
        api_key=b59117c083dfa1d4e6cc3186a568fd26
        # get your city id at https://openweathermap.org/find and replace
        city_id=2507480
        # you can use this "lang=ar" or replace with your language
        url="api.openweathermap.org/data/2.5/weather?id=${city_id}&appid=${api_key}&cnt=5&units=metric&lang=ar"
        curl ${url} -s -o $FileName
fi

#Name=$(cat $FileName | jq -r '.name')
#echo $Name

#MaxTemp=$(cat $FileName | jq '.main.temp_max' | awk '{print int($1+0.5)}')
#echo "${MaxTemp}°C"
convertArabicNum(){
 message=$1                    
message="${message//1/١}" 
message="${message//2/٢}" 
message="${message//3/٣}" 
message="${message//4/٤}" 
message="${message//5/٥}" 
message="${message//6/٦}" 
message="${message//7/٧}" 
message="${message//8/٨}" 
message="${message//9/٩}" 
message="${message//0/٠}" 
 echo "${message}"
}

case $1 in
HUMID)
        Humidity=$(cat $FileName | jq '.main.humidity')
       convertArabicNum ${Humidity}%
        ;;
WIND)
        WindSpeed=$(cat $FileName | jq '.wind.speed')
         convertArabicNum ${WindSpeed}
        ;;
DESCR)
        Description=$(cat $FileName | jq -r '.weather[0].description' | sed -e 's/\(.*\)/\U\1/')
        echo "${Description}"
        ;;
TEMP)
        Temp=$(cat $FileName | jq '.main.temp' | awk '{print int($1+0.5)}')
         convertArabicNum "${Temp}°"
        ;;
ICON)
        ICON=$(cat $FileName | jq -r '.weather[0].icon' | sed -e 's/\(.*\)/\U\1/')
        IMG=$(echo $ICON | tr '[:upper:]' '[:lower:]')
        echo src/images/$IMG.png
        ;;
*)
        echo "weather.sh [TEMP | ICON| DESCR | WIND| HUMID ]"
        ;;
esac

exit
