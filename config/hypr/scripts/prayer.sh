#!/bin/env bash

# -v = one line
# nothing =tow line
#
NextName=$(~/.local/bin/qprayer -Na)
NextTime=$(~/.local/bin/qprayer -n)
fajr=$(~/.local/bin/qprayer -f)
sunrise=$(~/.local/bin/qprayer -s)
dohr=$(~/.local/bin/qprayer -d)
asr=$(~/.local/bin/qprayer -a)
magreb=$(~/.local/bin/qprayer -m)
isha=$(~/.local/bin/qprayer -i)

if  [[ $1 == "-v" ]]; then
    echo -e "$NextTime حتى $NextName"
   
else
    echo -e "<b>$NextTime</b><span size='x-small'>\rحتى $NextName</span>"
   
fi
tooltip="الفجر :  $fajr \r"
tooltip+="الشروق: $sunrise \r"
tooltip+="الظهر : $dohr \r"
tooltip+="العصر : $asr \r"
tooltip+="المغرب: $magreb \r"
tooltip+="العشاء : $isha "
 echo -e $tooltip

if [[ "$NextTime" == "00:00" ]]; then
    notify-send -t 60000 -i "dialog-information" "qprayer" "حان وقت صلاة ${NextName}"
fi
