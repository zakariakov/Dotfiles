#!/bin/env bash

script=$(readlink -f $0)
path=$(dirname $script)

WAL_DIR=~/.config/hypr/settings/term
ROFI_THME=~/.config/rofi/themes/config-waltheme.rasi

#FILE=$(find "$WAL_DIR" -type f \( -iname "*.jpg" -o -iname "*.jpeg" -o -iname "*.png" \) | while read rfile; do
#    echo -en "$(basename $rfile .png)\x00icon\x1f${rfile}\n"
#done | rofi -show -dmenu -theme $ROFI_THME)
#

FILE=$(ls --file-type "$WAL_DIR" | grep -E '\.(png|jpg)$' | while read rfile; do
    echo -en "$(basename $rfile .png)\x00icon\x1f${WAL_DIR}/${rfile}\n"
done | rofi -show -dmenu -theme $ROFI_THME)

echo $FILE
Theme=$(basename "$FILE" .png)
#Theme=$BaseName #$(basename $BaseName .png)
Mode=$(echo $Theme | cut -d "." -f1)
ThemeName=$(echo $Theme | cut -d "." -f2)

echo $BaseName
echo $Theme
echo "Mode:" $Mode
echo "ThemeName:" $ThemeName

if [[ $Mode == Dark ]]; then
    notify-send "Rofi Theme" "theme selected $ThemeName dark mode"
    wal --theme $ThemeName
    $path/change_colors.sh "wal"

elif [[ $Mode == Light ]]; then
    notify-send "Rofi Theme" "theme selected $ThemeName light mode"
    wal -l --theme $ThemeName
    $path/change_colors.sh "wal"
else
    notify-send "Rofi Theme" "no theme selected"
fi
