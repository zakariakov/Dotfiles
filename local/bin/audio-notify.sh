#!/bin/bash

# Depend: "amixer" , "sox" for play sound
# You can call this script like this:
# $ ./audio-notify.sh up
# $ ./audio-notify.sh down
# $ ./audio-notify.sh mute

# Script modified from these wonderful people:
# https://github.com/dastorm/volume-notification-dunst/blob/master/volume.sh
# https://gist.github.com/sebastiencs/5d7227f388d93374cebdf72e783fbd6a
#dunst -c "$HOME/.config/dunst/dunstprogrc" &
function get_volume {
  #amixer get Master | grep '%' | head -n 1 | cut -d '[' -f 2 | cut -d '%' -f 1
  pamixer --get-volume
}

function is_mute {
#  amixer get Master | grep '%' | grep -oE '[^ ]+$' | grep off > /dev/null
  pamixer --get-mute
}

function send_notification {

volume=$(get_volume)
if [[ $(pamixer --get-mute) == true ]] ; then
	BC_ICON="$HOME/.local/bin/xoverlay/audio-mute.png"
	#bar=""

  else
  	BC_ICON="$HOME/.local/bin/xoverlay/audio-full.png"
  	
  	#barbgn=$(seq --separator="" 0 "$((volume / 5))" | sed 's/[0-9]//g')
  	#barend=$(seq --separator="" 0 "$((20-${#barbgn} ))" | sed 's/[0-9]//g')

  	#tr=奄奄奄奄奔奔奔奔奔奔奔墳墳墳墳
    #con=${Str:${#barbgn}:1}
	#upbar="$barbgn$barend"	
  	 
  fi


	ID=292
    #   bg=string:bgcolor:#b5cbd5 	
    #   fg=string:fgcolor:#1c1429
	
	# Send the notification
    #   dunstify  -i "$BC_ICON"  -r $ID -u low  "$volume%  $bar" 
	   #-h $bg -h $fg
	
       dunstify  -a "Audio-vol" -i "$BC_ICON" -r $ID  -u low "" -h int:value:$volume 
}

case $1 in
  up)
	# set the volume on (if it was muted)
	#amixer -D pulse set Master on > /dev/null
	# up the volume (+ 5%)
	#amixer -D pulse sset Master 5%+ > /dev/null
	pamixer -u -i 5 > /dev/null
	send_notification
    ;;
  down)
	#amixer -D pulse set Master on > /dev/null
	#amixer -D pulse sset Master 5%- > /dev/null
	
	pamixer -u -d 5 > /dev/null
	send_notification
    ;;
  mute)
	# toggle mute
	#amixer -D pulse set Master 1+ toggle > /dev/null
	pamixer -t
	send_notification
    ;;
esac

exit   
