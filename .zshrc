# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi


export TERM="xterm-256color"
export EDITOR=/usr/bin/vim

export TERMINAL=kitty
export PATH="${PATH}:${HOME}/.local/bin/"


##-- color man pages --##
export PAGER="less" 
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

ZSH_CACHE_DIR=$HOME/.cache/oh-my-zsh
if [[ ! -d $ZSH_CACHE_DIR ]]; then 
  mkdir $ZSH_CACHE_DIR
fi
ZSH=~/.oh-my-zsh


typeset -g POWERLEVEL9K_INSTANT_PROMPT=off

##af-magic ,adben ,strug
#ZSH_THEME="af-magic"
source ~/.p10k.zsh
 ZSH_THEME="powerlevel10k/powerlevel10k"

source  $ZSH/oh-my-zsh.sh
source  /usr/share/fzf/key-bindings.zsh
source  /usr/share/fzf/completion.zsh
source  ~/.config/zshalias
source  /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source  /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
plugins=(git fzf archlinux colored-man-pages colorize)
neofetch