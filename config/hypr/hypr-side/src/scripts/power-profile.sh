#!/bin/env bash

CURENT=$(powerprofilesctl get)

if [[ $1 == set ]]; then
    case $CURENT in
    power-saver)
        echo '' > /tmp/power-profile.txt
        echo ''

        powerprofilesctl set balanced
    ;;
    balanced)
        echo ''  > /tmp/power-profile.txt
        echo ''

        powerprofilesctl set performance
    ;;
    performance)
        echo '' > /tmp/power-profile.txt
        echo ''
        powerprofilesctl set power-saver
    ;;
    esac
else
    case $CURENT in
    power-saver)
        echo '' > /tmp/power-profile.txt
        echo ''
    ;;
    balanced)
          echo ''  > /tmp/power-profile.txt
          echo ''
    ;;
    performance)
        echo ''  > /tmp/power-profile.txt
        echo ''
    ;;
    esac
fi



exit 0
