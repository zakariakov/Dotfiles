#!/bin/bash

FILE=~/.cache/xfce4/notifyd/log

n=false
c=0
ICON=""
BODY=""
SUMMARY=""
Head="(box :class  \"NotifyBox\" :orientation \"v\" :hxpand false :vxpand false :space-evenly \"false\" :spacing 5"
Notif="(box :class  \"Notif\" :orientation \"h\" :hxpand false :space-evenly \"false\"  :spacing 5 "
Box="(box :orientation \"v\" :hxpand false :space-evenly \"false\" "

tac $FILE | while read line; do
        if [[ $line == "app_icon"* ]]; then
                ICON=$(echo $line | awk '{ print substr( $0, 10 ) }')
                #echo "icon:" $ICON
                boxIcon=" ( box :class \"Icon\" :wrap true :halign \"start\"  :style \"background-image: url('${ICON}');\")"

        elif [[ $line == "body"* ]]; then
                BODY=$(echo $line | awk '{ print substr( $0, 6 ) }')
                boxBody="  (label :class \"Body\"  :wrap true  :halign \"start\"  :limit-width 30  :text '${BODY}')"

        elif [[ $line == "summary"* ]]; then
                SUMMARY=$(echo $line | awk '{ print substr( $0, 9) }')
                wrd="\\n"
               SUMMARY=${SUMMARY//$wrd/}
               # FOO=${FOO//$WORDTOREMOVE/}
                boxSummary=" (label :class \"Sumary\" :wrap true  :halign \"start\"  :hxpand false :limit-width 30 :text '${SUMMARY}')"

        elif [[ $line == "app_name"* ]]; then
                APPNAME=$(echo $line | awk '{ print substr( $0, 10) }')
                boxApp=" (label :class \"AppName\"  :wrap true :halign \"start\"  :limit-width 30 :text '${APPNAME}')"

        elif [[ $line == "["*"]" ]]; then
                n=true
        fi

        if [[ $n == true ]]; then
                Result+="  ${Notif} ${boxIcon} ${Box} ${boxApp} ${boxBody} ${boxSummary}))"
                echo "${Result}" >/tmp/xfcnotify.txt
                n=false
                c=$((c + 1))
                ICON=""
                BODY=""
                APPNAME=""
                SUMMARY=""

        fi

        if [[ $c == 4 ]]; then
                break
        fi

done
Result=$(cat /tmp/xfcnotify.txt)
echo "${Head} ${Result})"
