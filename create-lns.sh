#/bin/bash

cd $HOME

## Dirs
ln -s -f ~/.Dotfiles/.vimrc .vimrc
ln -s -f ~/.Dotfiles/.mpd .mpd
ln -s -f ~/.Dotfiles/.ncmpcpp .ncmpcpp
#ln -s -f ~/.Dotfiles/.icons  .icons
#ln -s -f ~/.Dotfiles/.temes  .themes

## files 
ln -s -f ~/.Dotfiles/.vim .vim
ln -s -f ~/.Dotfiles/.gtkrc-2.0 .gtkrc-2.0
ln -s -f ~/.Dotfiles/.bashrc .bashrc
ln -s -f ~/.Dotfiles/.zshrc .zshrc
ln -s -f ~/.Dotfiles/.profile .profile
ln -s -f ~/.Dotfiles/.profile .zprofile
cp -f ~/.Dotfiles/.Xresources .Xresources
mkdir -p ~/.cache/wal
cp ~/.Dotfiles/cache/wal/colors-waybar.css ~/.cache/wal/colors-waybar.css
cd ~/.config/

## Dirs
ln -s -f ~/.Dotfiles/config/dunst dunst
#ln -s -f ~/.Dotfiles/config/eww eww
ln -s -f ~/.Dotfiles/config/rofi rofi
ln -s -f ~/.Dotfiles/config/ranger ranger
ln -s -f ~/.Dotfiles/config/gtk-2.0 gtk-2.0
ln -s -f ~/.Dotfiles/config/gtk-3.0 gtk-3.0
ln -s -f ~/.Dotfiles/config/gtk-4.0 gtk-4.0
ln -s -f ~/.Dotfiles/config/qt5ct qt5ct
ln -s -f ~/.Dotfiles/config/fontconfig fontconfig
ln -s -f ~/.Dotfiles/config/zathura zathura
ln -s -f ~/.Dotfiles/config/hypr hypr
#ln -s -f ~/.Dotfiles/config/swaylock swaylock
ln -s -f ~/.Dotfiles/config/wlogout wlogout
ln -s -f ~/.Dotfiles/config/nwg-dock-hyprland nwg-dock-hyprland
ln -s -f ~/.Dotfiles/config/neofetch neofetch
ln -s -f ~/.Dotfiles/config/waybar waybar

## Terminal
ln -s -f ~/.Dotfiles/config/kitty kitty
#ln -s -f ~/.Dotfiles/config/wezterm wezterm

## Wm
ln -s -f ~/.Dotfiles/config/hypr hypr
#ln -s -f ~/.Dotfiles/config/i3 i3
#ln -s -f ~/.Dotfiles/config/openbox openbox
#ln -s -f ~/.Dotfiles/config/awesome awesome
#ln -s -f ~/.Dotfiles/config/herbstluftwm herbstluftwm
#ln -s -f ~/.Dotfiles/config/bspwm bspwm
#ln -s -f ~/.Dotfiles/config/dk dk

#ln -s -f ~/.Dotfiles/config/wayfire wayfire

## files 
#ln -s -f ~/.Dotfiles/config/picom.conf picom.conf
#ln -s -f ~/.Dotfiles/config/picom-i3.conf picom-i3.conf
ln -s -f ~/.Dotfiles/config/zshalias zshalias
#ln -s -f ~/.Dotfiles/config/wayfire/wayfire.ini wayfire.ini
#ln -s -f ~/.Dotfiles/config/wayfire/wf-shell.ini wf-shell.ini
cd $HOME

exit 0
