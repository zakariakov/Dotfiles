#!/usr/bin/env bash



function json() {
  jq --unbuffered --null-input --compact-output \
    --arg text "$1" \
    --arg class "$2" \
    '{"text": $text, "class": $class}'
}

pacman_updates=$(checkupdates)

pacman_updates_count=$(echo "$pacman_updates" | grep -vc ^$)


 if [ "$pacman_updates_count" -gt 0 ]; then
    json $pacman_updates_count  "pending-updates"
  else
    json "" "updated" 
  fi

