#!/bin/env bash
#rofi -modi 'run' -show run -location 2 -theme ~/.config/rofi/themes/popmenu.rasi
ANS=$(echo "Screen|Window|region|" | \
                rofi -sep "|" -dmenu -i -p 'System: ' ""  -lines 5 \
                -location 2 -theme ~/.config/rofi/themes/popmenu.rasi)
            case "$ANS" in
                *Screen) hyprshot -m output ;;
                *Window) hyprshot -m window ;;
                *region) hyprshot -m region ;;
                *) exit ;;
            esac