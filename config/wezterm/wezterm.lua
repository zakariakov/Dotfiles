local wezterm = require("wezterm")
local act = wezterm.action
-- local launch_menu = {}
-- local default_shell = "/bin/zsh"
local padding = {
    left = "2cell",
    right = "2cell",
    top = "1cell",
    bottom = "1cell"
}

---- Reload the configuration every ten minutes
-- wezterm.time.call_after(600, function()
--	wezterm.reload_configuration()
-- end)

-- A helper function for my fallback fonts
local function font_with_fallback(name, params)
    local names = {name,"Hack Nerd Font","Vazir","Noto Color Emoji"}
    return wezterm.font_with_fallback(names, params)
end

-- local function get_theme()
--	local _time = os.date("*t")
--	if _time.hour >= 1 and _time.hour < 9 then
--		return "Rosé Pine (base16)"
--	elseif _time.hour >= 9 and _time.hour < 17 then
--		return "tokyonight_night"
--	elseif _time.hour >= 17 and _time.hour < 21 then
--		return "Catppuccin Mocha"
--	elseif _time.hour >= 21 and _time.hour < 24 or _time.hour >= 0 and _time.hour < 1 then
--		return "kanagawabones"
--	end
-- end

return {
  enable_wayland = false,
    bidi_enabled = true,
    bidi_direction = "AutoLeftToRight",
 color_scheme = 'wal',
    tab_bar_at_bottom = false,
    use_fancy_tab_bar = true,
    hide_tab_bar_if_only_one_tab = true,

    font = font_with_fallback({
        family = "jetBrains Mono",
        harfbuzz_features = {"zero"},
        weight = "Medium"
    }),
    font_rules = {
        {
            intensity = "Bold",
            font = font_with_fallback({
                family = "jetBrains Mono",
                harfbuzz_features = {"zero"},
                weight = "Medium"
            })
        }, {
            italic = true,
            intensity = "Bold",
            font = font_with_fallback({
                family = "jetBrains Mono",
                harfbuzz_features = {"zero"},
                weight = "Medium",
                italic = true
            })
        }, {
            italic = true,
            font = font_with_fallback({
                family = "jetBrains Mono",
                harfbuzz_features = {"zero"},
                weight = "Regular",
                italic = true
            })
        }
    },
   -- initial_cols = 84,
   -- initial_rows = 16,
    use_dead_keys = false,
    window_padding = padding,
  --  window_decorations = "TITLE",

    selection_word_boundary = " \t\n{}[]()\"'`,;:@",
    -- disable_default_key_bindings = true,
    line_height = 1,
    font_size = 12,
    window_background_opacity = 0.95,
    bold_brightens_ansi_colors = false,
    -- swap_backspace_and_delete = false,
    -- term = "wezterm",
    -- freetype_load_target = "Light",
 warn_about_missing_glyphs = false,
  enable_kitty_graphics = true,

    keys = {
        {
            key = 'v',
            mods = 'ALT|CTRL',
            action = act.SplitVertical {domain = 'CurrentPaneDomain'}
        }, {
            key = 'h',
            mods = 'ALT|CTRL',
            action = act.SplitHorizontal {domain = 'CurrentPaneDomain'}
        }

    }

}

