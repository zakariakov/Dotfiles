" The default vimrc file.
"
" Maintainer:	Bram Moolenaar <Bram@vim.org>
" Last change:	2019 Oct 27
"
" This is loaded if no vimrc file was found.
" Except when Vim is run with "-u NONE" or "-C".
" Individual settings can be reverted with ":set option&".
" Other commands can be reverted as mentioned below.

" plugins {{{

set t_Co=256


call plug#begin ('~/.vim/plugged') " ----------------

	"Plug 'vim-airline/vim-airline'
	"Plug 'vim-airline/vim-airline-themes'
	Plug 'lilydjwg/colorizer'
	Plug 'kabbamine/vcoolor.vim'
	Plug 'gko/vim-coloresque'
	
	Plug 'ryanoasis/vim-devicons'
	"Plug 'shougo/neocomplete.vim'
	Plug 'shougo/vimshell.vim'
	Plug 'scrooloose/nerdtree'
	Plug 'vimwiki/vimwiki'

call plug#end()      	" ---------------------------

 
 " Map cntl j for down k for up h for left l for right 
 map <C-j> <C-W>j 
 map <C-k> <C-W>k 
 map <C-h> <C-W>h
 map <C-l> <C-W>l

" " To open a new empty buffer
" "  This replaces :tabnew which I used to bind to this mapping
 nmap <leader>T :enew<cr>

" "  Move to the next buffer
 nmap <leader>l :bnext<CR>

" " Move to the previous buffer
 nmap <leader>h :bprevious<CR>

" " Close the current buffer and move to the previous one
" " This replicates the idea of closing a tab
 nmap <leader>bq :bp <BAR> bd #<CR>



" Enable the list of buffers
 let g:airline#extensions#tabline#enabled = 1

set laststatus=2

" }}}

" Costom Plug {{{
map <C-n> :NERDTreeToggle<CR>
" Snippets ---------------------
" Trigger configuration. You need to change this to something else than <tab>
" if you use https://github.com/Valloric/YouCompleteMe.
 let g:UltiSnipsExpandTrigger="<tab>"
 let g:UltiSnipsJumpForwardTrigger="<c-b>"
 let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" " If you want :UltiSnipsEdit to split your window.
 let g:UltiSnipsEditSplit="vertical"

" Airline Statu ---------------
"let g:airline_powerline_fonts = 1
"let g:airline_statusline_ontop=1


" LIGHTLINE -----------------------
let g:lightline = {'colorscheme': 'nord', }
let g:lightline.separator = { 'left': '', 'right': '' }

let g:lightline.subseparator = { 'left': '', 'right': '' }

" whether or not to show the nerdtree brackets around flags
 let g:webdevicons_conceal_nerdtree_brackets = 1

" enable folder/directory glyph flag (disabled by default with 0)
 let g:WebDevIconsUnicodeDecorateFolderNodes = 1

" loading the plugin
 let g:webdevicons_enable = 1

" adding the flags to NERDTree
 let g:webdevicons_enable_nerdtree = 1

 " enable folder/directory glyph flag (disabled by default with 0)
 let g:WebDevIconsUnicodeDecorateFolderNodes = 1



" unicode symbol --------------
"let g:airline_left_sep=''
"let g:airline_right_sep=''
"let g:airline_theme='wombat'
let g:vcoolor_map = '<C-C>'


colorscheme default


" }}}
" Orig Vim {{{
" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
  finish
endif

" Bail out if something that ran earlier, e.g. a system wide vimrc, does not
" want Vim to use these default values.
if exists('skip_defaults_vim')
  finish
endif

" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
" Avoid side effects when it was already reset.
if &compatible
  set nocompatible
endif

" When the +eval feature is missing, the set command above will be skipped.
" Use a trick to reset compatible only when the +eval feature is missing.
silent! while 0
  set nocompatible
silent! endwhile

" Allow backspacing over everything in insert mode.
set backspace=indent,eol,start

set history=200		" keep 200 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set wildmenu		" display completion matches in a status line

set ttimeout		" time out for key codes
set ttimeoutlen=100	" wait up to 100ms after Esc for special key

" Show @@@ in the last line if it is truncated.
set display=truncate

" Show a few lines of context around the cursor.  Note that this makes the
" text scroll if you mouse-click near the start or end of the window.
set scrolloff=5

" Do incremental searching when it's possible to timeout.
if has('reltime')
  set incsearch
endif

" Do not recognize octal numbers for Ctrl-A and Ctrl-X, most users find it
" confusing.
set nrformats-=octal

" For Win32 GUI: remove 't' flag from 'guioptions': no tearoff menu entries.
if has('win32')
  set guioptions-=t
endif

" Don't use Ex mode, use Q for formatting.
" Revert with ":unmap Q".
map Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
" Revert with ":iunmap <C-U>".
inoremap <C-U> <C-G>u<C-U>

" In many terminal emulators the mouse works just fine.  By enabling it you
" can position the cursor, Visually select and scroll with the mouse.
" Only xterm can grab the mouse events when using the shift key, for other
" terminals use ":", select text and press Esc.
if has('mouse')
  if &term =~ 'xterm'
    set mouse=a
  else
    set mouse=nvi
  endif
endif

" Switch syntax highlighting on when the terminal has colors or when using the
" GUI (which always has colors).
if &t_Co > 2 || has("gui_running")
  " Revert with ":syntax off".
  syntax on

  " I like highlighting strings inside C comments.
  " Revert with ":unlet c_comment_strings".
  let c_comment_strings=1
endif

" Only do this part when Vim was compiled with the +eval feature.
if 1

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  " Revert with ":filetype off".
  filetype plugin indent on

  " Put these in an autocmd group, so that you can revert them with:
  " ":augroup vimStartup | au! | augroup END"
  augroup vimStartup
    au!

    " When editing a file, always jump to the last known cursor position.
    " Don't do it when the position is invalid, when inside an event handler
    " (happens when dropping a file on gvim) and for a commit message (it's
    " likely a different one than last time).
    autocmd BufReadPost *
      \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
      \ |   exe "normal! g`\""
      \ | endif

  augroup END

endif

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not definedzalready.
" Revert with: ":delcommand DiffOrig".
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif

if has('langmap') && exists('+langremap')
  " Prevent that the langmap option applies to characters that result from a
  " mapping.  If set (default), this may break plugins (but it's backward
  " compatible).
  set nolangremap
endif


" }}}

" my costum config {{{
set number
set ignorecase
set hlsearch

"########################################
autocmd BufWinLeave * mkview
autocmd BufWinEnter * silent loadview

autocmd FileType css set omnifunc=csscomplete#CompleteCSS


" }}}

" Help  {{{
" ctrl+n   toggle tree
"line nuber ------------------------------------
"set rnu!  nornu! #show/hide number under cursor
"set nu    nonu   #show numbers /hide numbers		
"auto complet css ------------------------------
"ctrl +x  ctrl + o
"auto complet any------------------------------
"ctrl +n 
" line folding --------------------------------
"  auto save line folding
"  mkview creat folder view
"  z-f creat line fold
"  z-o open  line fold
"  z-c colapse line fold
"  z-N colapse all
"  z-n open all
"  z-d delete fold
"  z-r open all
"  z-m colapse all
"  z-f-a-[ creat fold betwen '[]' ex
"  
" Run a Command From VIM - BASH --------------
"  :! myscript.sh
"
" AUTO COMPLZTION -----------------------------
" git clone git://github.com/ajh17/VimCompletesMe.git ~/.vim/pack/vendor/start/VimCompletesMe
" ctrl+x 
" Vim's local keyword completion (Ctrl-X_Ctrl-N)
" File path completion when typing a path (Ctrl-X_Ctrl-F)
" Dictionary words (Ctrl-X_Ctrl-K)
" User-defined completion (Ctrl-X_Ctrl-U)
" Tags (Ctrl-X_Ctrl-])
" Omni completion (Ctrl-X_Ctrl-O)
" Replace All Strings----------------------------
" :%s/Str/NewStr/g
"
"  }}}
