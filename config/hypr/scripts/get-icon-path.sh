#!/bin/env bash

ICON=$1
#THEME=$(gsettings get org.gnome.desktop.interface icon-theme | tr -d "'")
THEME=McMojave-circle
#echo "THEME: $THEME/$ICON"
HOME_THEME_PATH=$HOME/.icons
LOCAL_THEME_PATH=$HOME/.local/share/icons/
USERS_THEME_PATH=/usr/share/icons/
HICOLOR_THEME_PATH=/usr/share/icons/hicolor
TEMPICONS=/tmp/dockicon/
DEFAULT_PATH=$HOME/.config/waybar/icons


# TODO--------------FIX---------------

if [ ! -d $TEMPICONS ]; then
   mkdir -p $TEMPICONS
fi

function find_icon() {

        RESULT=$(find $1 -name "${ICON}*" | sort -R | head -1)
        if [ ! -z $RESULT ]; then
                if [ -f $RESULT ]; then
                     #   echo "RESULT: $RESULT"
                       ln -s -f $RESULT $DEFAULT_PATH/$ICON
                       echo $RESULT
                        exit
                fi
        fi
}
#echo "THEME: $HOME_THEME_PATH/$THEME/apps"
if [ -d "$HOME_THEME_PATH/$THEME/apps" ]; then
        find_icon find $HOME_THEME_PATH/$THEME/apps
fi

#echo "THEME: $HOME_THEME_PATH/$THEME/"
if [ -d "$HOME_THEME_PATH/$THEME" ]; then
        INHHRETS=$(cat $HOME_THEME_PATH/$THEME/index.theme | grep Inherits | cut -d "," -f1 | cut -d "=" -f2)
     #   echo "THEME: $HOME_THEME_PATH/$INHHRETS"
        find_icon $HOME_THEME_PATH/$THEME
fi

if [ -d "$LOCAL_THEME_PATH/$THEME/apps" ]; then
        find_icon $LOCAL_THEME_PATH/$THEME/apps
fi
if [ -d "$LOCAL_THEME_PATH/$THEME" ]; then
        INHHRETS=$(cat $LOCAL_THEME_PATH/$THEME/index.theme | grep Inherits | cut -d "," -f1 | cut -d "=" -f2)
        find_icon $LOCAL_THEME_PATH/$THEME
fi
if [ -d "$USERS_THEME_PATH/$THEME" ]; then
        INHHRETS=$(cat USERS_THEME_PATH/$THEME/index.theme | grep Inherits | cut -d "," -f1 | cut -d "=" -f2)
        find_icon $USERS_THEME_PATH/$THEME
fi

## INHHRETS
if [ -d "$HOME_THEME_PATH/$INHHRETS" ]; then
        find_icon $HOME_THEME_PATH/$INHHRETS
fi
if [ -d "$LOCAL_THEME_PATH/$INHHRETS" ]; then
        find_icon $LOCAL_THEME_PATH/$INHHRETS
fi
if [ -d "$USERS_THEME_PATH/$INHHRETS" ]; then
        find_icon $USERS_THEME_PATH/$INHHRETS
fi

if [ -d "$HICOLOR_THEME_PATH" ]; then
        find_icon $HICOLOR_THEME_PATH
fi
if [ -d "/usr/share/pixmaps" ]; then
        find_icon /usr/share/pixmaps
fi
#if test -d $HOME_THEME_PATH/$THEME/apps; then

# RESULT=$(find $HOME_THEME_PATH/$THEME/apps -name "${ICON}*" | sort -R | head -1)
# if test -f $RESULT; then
# ln -s -f $RESULT $DEFAULT_PATH/$ICON
# echo $RESULT
# exit
# fi
#fi

#if test -d $HOME_THEME_PATH/$THEME; then

#RESULT=$(find $HOME_THEME_PATH/$THEME -name "${ICON}*" | sort -R | head -1)
#echo "RESULT : $RESULT"
#if test -f $RESULT; then
#        ln -s -f $RESULT $DEFAULT_PATH/$ICON
#        echo $RESULT
#        exit
#fi
#fi

#if test -d $LOCAL_THEME_PATH/$THEME/apps; then

# RESULT=$(find $LOCAL_THEME_PATH/$THEME/apps -name "${ICON}*" | sort -R | head -1)
# if test -f $RESULT; then
# ln -s -f $RESULT $DEFAULT_PATH/$ICON
# echo $RESULT
# exit
# fi
#fi

#if test -d $LOCAL_THEME_PATH/$THEME; then
# RESULT=$(find $LOCAL_THEME_PATH/$THEME -name "${ICON}*" | sort -R | head -1)
# echo "RESULT : $RESULT"
# if test -f $RESULT; then
# ln -s -f $RESULT $DEFAULT_PATH/$ICON
# echo $RESULT
# exit
# fi
#fi

#if test -d $USERS_THEME_PATH/$THEME; then

# RESULT=$(find $USERS_THEME_PATH/$THEME -name "${ICON}*" | sort -R | head -1)
# echo "RESULT : $RESULT"
# if test -f $RESULT; then
# ln -s -f $RESULT $DEFAULT_PATH/$ICON
# echo $RESULT
# exit
# fi
#fi

#if test -d $HICOLOR_THEME_PATH; then

# RESULT=$(find $HICOLOR_THEME_PATH -name "${ICON}*" | sort -R | head -1)
# echo "RESULT : $RESULT"
# if test -f $RESULT; then
# ln -s -f $RESULT $DEFAULT_PATH/$ICON
# echo $RESULT
# exit
# fi
#fi

#if test -d /usr/share/pixmaps/; then

# RESULT=$(find /usr/share/icons -name "${ICON}*" | sort -R | head -1)
# echo "RESULT : $RESULT"
# if test -f $RESULT; then
# ln -s -f $RESULT $DEFAULT_PATH/$ICON
# echo $RESULT
# exit
# fi
#fi

# ln -s -f $DEFAULT_PATH/${ICON}.svg $DEFAULT_PATH/$ICON
# if [ -f $DEFAULT_PATH/${ICON}.svg ]; then
#         echo $DEFAULT_PATH/${ICON}.svg
#            ln -s -f  $DEFAULT_PATH/${ICON}.svg $TEMPICONS/$ICON
# elif [ -f $DEFAULT_PATH/${ICON}.png ]; then
#            ln -s -f  $DEFAULT_PATH/${ICON}.png  $TEMPICONS/$ICON
# else
#            ln  $DEFAULT_PATH/x-app.svg $TEMPICONS/$ICON
# fi
exit

