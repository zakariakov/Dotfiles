#!/bin/env bash

    if [ ! -z $(pgrep hypridle) ]; then
        echo 'btn_On'  > /tmp/idle.txt
        killall hypridle

    else
        echo 'btn_Off'  > /tmp/idle.txt
        setsid hypridle

    fi
exit 0
