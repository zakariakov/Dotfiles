#!/bin/env bash

G="\e[1;32m"
B="\e[1;34m"
N="\e[1;0m"
R="\e[1;36m"
I="\e[3m"
#-------------------------------------------------
printf "${G} ╻ ╻┏━┓╻ ╻┏┓ ┏━┓┏━┓   ╺┳╸╻ ╻┏━╸┏┳┓┏━╸\n"
printf "${G} ┃╻┃┣━┫┗┳┛┣┻┓┣━┫┣┳┛    ┃ ┣━┫┣╸ ┃┃┃┣╸ \n"
printf "${G} ┗┻┛╹ ╹ ╹ ┗━┛╹ ╹╹┗╸    ╹ ╹ ╹┗━╸╹ ╹┗━╸\n"
#-------------------------------------------------
echo ""
NUM=$1
HYPR_PATH="$HOME/.config/hypr"
BAR_PATH="$HOME/.config/waybar"
THEME_PATH="$BAR_PATH/themes"

function errore_Theme() {
    echo "Dir or file no exist :"
    echo $1
    exit 1
}

Select_Theme() {
  #  printf "${B}Enter the num folder toswich [01-04] ${N}:"
  #  read NUM

    
    [[ ! -f "$THEME_PATH/$NUM/config.jsonc" ]] && errore_Theme "$THEME_PATH/$NUM/config.jsonc" 
  #  [[ ! -f "$THEME_PATH/$NUM/modules.jsonc" ]] && Erore_Theme "$THEME_PATH/$NUM/modules.jsonc"
    [[ ! -f "$THEME_PATH/$NUM/style.css" ]] && Erore_Theme "$THEME_PATH/$NUM/style.css"

    ln -s -f "$THEME_PATH/$NUM/config.jsonc" "$BAR_PATH/config.jsonc"
   # ln -s -f "$THEME_PATH/$NUM/modules.jsonc" "$BAR_PATH/modules.jsonc"
    ln -s -f "$THEME_PATH/$NUM/style.css" "$BAR_PATH/style.css"

    setsid $HYPR_PATH/scripts/statusbar r   > /dev/null

}

Select_Theme

exit 0
