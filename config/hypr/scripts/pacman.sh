#!/bin/env bash

VALUE=$(checkupdates | wc -l)

if [[ "$VALUE" == "0" ]]; then
  echo '󰧵'
else
  echo "󰊠 $VALUE"
fi
