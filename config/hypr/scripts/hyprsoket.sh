#!/usr/bin/env bash
#LAUNCHER=$1
#~/.config/hypr/scripts/get-icon-path.sh $1

#case $LAUNCHER in
#Terminal) APP_CLASS=org.wezfurlong.wezterm ;;
#web-browser-app) APP_CLASS=Brave-browser ;;
#Filemanager) APP_CLASS=Thunar ;;
#text-editor) APP_CLASS=mousepad ;;
#esac

#DEFAULT_PATH=$HOME/.config/waybar/icons
#MICON=$DEFAULT_PATH/$1
#function json() {
#    jq --unbuffered --null-input --compact-output \
#        --arg text "$1" \
#        --arg tooltip "$2" \
#        --arg class "$3" \
#        '{"text": $text, "tooltip": $tooltip, "class": $class}'
#}

handle() {
    LINE=$1
    echo $LINE
    case $LINE in
    openwindow*)
  #  echo "openwindow"
     pkill -RTMIN+8 waybar
        #   has_launcher=$(echo $1LINE | grep $APP_CLASS)
        #   if [[ ${has_launcher} > /dev/null ]]; then
        #     json "" " " "running"
        #   fi
        ;;
    closewindow*)
        sleep  0.5s
      #  echo "closewindow"
       pkill -RTMIN+8  waybar
    
      #  has_launcher=$(hyprctl clients | grep $APP_CLASS)
      #  if [[ ! -z ${has_launcher} ]]; then
      #      json "" " " "running"
      #  else
      #      json "$MICON" " $APP_CLASS" "$LAUNCHER"
      #  fi
      #  ;;
    esac
}

## on start only
#has_launcher=$(hyprctl clients | grep $APP_CLASS)
#if [[ ! -z ${has_launcher} ]]; then
#    json "" " " "running"
#else
#    json "$MICON" " $APP_CLASS" "$LAUNCHER"
#fi

#socat -U - UNIX-CONNECT:/tmp/hypr/$HYPRLAND_INSTANCE_SIGNATURE/.socket2.sock | while read -r line; do handle "$line"; done

socat -U - UNIX-CONNECT:$XDG_RUNTIME_DIR/hypr/$HYPRLAND_INSTANCE_SIGNATURE/.socket2.sock | while read -r line; do handle "$line"; done
