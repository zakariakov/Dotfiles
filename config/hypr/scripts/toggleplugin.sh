#!/bin/env bash
Arg=$1

_SELECT_PLUGIN() {
    case $1 in

    bar)
        [[ -z $(hyprctl plugin list | grep hyprbars) ]] && hyprpm enable hyprbars || hyprpm disable hyprbars
        ;;
    border)
        [[ -z $(hyprctl plugin list | grep borders-plus-plus) ]] && hyprpm enable borders-plus-plus || hyprpm disable borders-plus-plus
        ;;
    trails)
        [[ -z $(hyprctl plugin list | grep hyprtrails) ]] && hyprpm enable hyprtrails || hyprpm disable hyprtrails
        ;;
    winwrap)
        [[ -z $(hyprctl plugin list | grep hyprwinwrap) ]] && hyprpm enable hyprwinwrap || hyprpm disable hyprwinwrap
        ;;
    hycov)
        [[ -z $(hyprctl plugin list | grep hyprexpo) ]] && hyprpm enable hyprexpo || hyprpm disable hyprexpo
        ;;
    esac
}

_ROFI_PLUGINS() {
    ANS=$(echo "bar|border|trails|winwrap|hyprexpo" |
        rofi -sep "|" -dmenu -i -p 'theme:' "" -markup-rows -location 2 \
            -theme ~/.config/rofi/themes/popmenu.rasi)
    echo "$ANS"
    _SELECT_PLUGIN $ANS
}
case $Arg in
--rofi) _ROFI_PLUGINS ;;
*) _SELECT_PLUGIN $Arg ;;
esac
