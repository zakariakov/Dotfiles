#!/bin/bash

[ -z $1 ] && DURATION=30 || DURATION=$1

DURATION=$(($DURATION * 60))

WAL_DIR=$HOME/wallpaper
[ -z $WAL_DIR ] && WAL_DIR=$HOME/.config/hypr/wallpapers

#echo $DURATION

image_blur() {
    _W=$(hyprctl -j monitors | jq '.[] | select(.focused==true) | .width')
    _H=$(hyprctl -j monitors | jq '.[] | select(.focused==true) | .height')
    magick $FILE -resize $_Wx$_H -blur 0x8 $HOME/.cache/blured.png
    ln -s -f "${FILE}" $HOME/.cache/background
}

randomWall() {
    FILE=$(find $WAL_DIR -type f \( -iname '*.PNG' -o -iname '*.JPG' -o -iname '*.JPEG' -o -iname '*.webp' \) | shuf -n 1)
    [ ! -f $FILE ] && FILE=$HOME/.cache/background

    Size=$(identify $FILE | awk '{print $3}')

    WW=$(echo $Size | cut -d "x" -f1)
    HH=$(echo $Size | cut -d "x" -f2)


     echo $FILE $Size

    if [[ ("$WW" -le "800") || ("$HH" -le "500") ]]; then
        #  if ($WW <= 800) || ($HH <= 500); then
        echo "small image:  $WW $HH"

        swww img --no-resize "$FILE" --transition-type random \
            --transition-fps 60 \
            --transition-duration 2
    else

        swww img "$FILE" --transition-type random \
            --transition-fps 60 \
            --transition-duration 2
    fi
    image_blur
}

while true; do

    randomWall
    sleep $DURATION

done
