#!/bin/env bash

# Generates zathura configuration file

. $HOME/.cache/wal/colors.sh

cat <<CONF
[colors]
ansi = [
	'$color0',
	'$color1',
	'$color2',
	'$color3',
	'$color4',
	'$color5',
	'$color6',
	'$color7',
]
background = '$background'
brights = [
	'$color8',
	'$color9',
	'$color10',
	'$color11',
	'$color12',
	'$color13',
	'$color14',
	'$color15',
]
cursor_bg = '$foreground'
cursor_border = '$foreground'
cursor_fg = '$background'
foreground = '$foreground'
[metadata]
name = 'wal'
CONF
