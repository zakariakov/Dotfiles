#!/usr/bin/env bash


## Copyright (C) 2020-2022 Aditya Shakya <adi1090x@gmail.com>

## wlogout with alt layout and style file
DESKTOP=$1
LAYOUT="$HOME/.config/wlogout/layout"
STYLE="$HOME/.config/wlogout/style.css"

# detect monitor res

if [[ "$DESKTOP" == "h" ]]; then
###-- HyprLand
	x_mon=$(hyprctl -j monitors | jq '.[0]| .width')
	y_mon=$(hyprctl -j monitors | jq '.[0]| .height')
elif  [[ "$DESKTOP" == "w" ]]; then
##--Wayfire
	RANDR=$(wlr-randr | grep current  |  awk '{print $1}')
	RANDR=$(echo -n $RANDR |  awk '{print $1}')
        x_mon=$(echo -n $RANDR | cut -d "x" -f1)
	y_mon=$(echo -n $RANDR | cut -d "x" -f2)
else
##--X11
	RANDR=$(xrandr -q | grep 'connected primary'| awk '{print $4}'  | cut -d "+" -f1)
	x_mon=$(echo -n $RANDR | cut -d "x" -f1)
	y_mon=$(echo -n $RANDR | cut -d "x" -f2)
fi

echo "X=" $x_mon "Y=" $y_mon
if [[ ! `pidof wlogout` ]]; then
	wlogout --layout ${LAYOUT} --css ${STYLE} \
		--buttons-per-row 2 \
		--column-spacing 0 \
		--row-spacing 0 \
		--margin-top $(( y_mon  / 4 )) \
		--margin-bottom $(( y_mon  / 4 )) \
		--margin-left $(( x_mon  / 3 )) \
		--margin-right $(( x_mon  / 3 ))
else
	pkill wlogout
fi
