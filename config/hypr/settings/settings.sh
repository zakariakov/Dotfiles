#!/bin/env bash
#--------------------------------------
# depends:gum, yad, readlink, nohup (coreutils)

LC_ALL=en_US.UTF-8
script=$(readlink -f $0)
path=$(dirname $script)
H_N=12
BACKGROUND="#242424"
VAR_FILE=~/.config/hypr/variables.conf
SCRIPT_Path=~/.config/hypr/scripts
Hyper_Anim_Path=~/.config/hypr/animations
WAYBAR_PATH=~/.config/waybar
PYWAL_THEMES=/usr/lib/python3.13/site-packages/pywal/colorschemes/light
## ($1)key '=' ($2)value ($3)file
function set_varConfig() {
    sed -i "s/^\(\s*$1\s*=\s*\).*\$/\1$2/" $VAR_FILE
}

## ($1)key '=' ($2)value ($3)file
function set_configH() {
    sed -i "s/^\(\s*$1\s*=\s*\).*\$/\1$2/" "$3"
}

function calculate_position() {
    read < <(stty </dev/tty size) \
        TERMINAL_LINES TERMINAL_COLUMNS
    _H=$((TERMINAL_LINES - 7))
    _W=$((TERMINAL_COLUMNS - 4))
    H_N=$_H
}
function setgtk_theme() {
    if [[ $1 == "-l" ]]; then
        THEME=adw-gtk3
        PREF=prefer-lightt
        GPD=0
    else
        THEME=adw-gtk3-dark
        PREF=prefer-dark
        GPD=1
    fi
    GTK3_SETTING_FILE=~/.config/gtk-3.0/settings.ini
    GTK4_SETTING_FILE=~/.config/gtk-4.0/settings.ini

    SCHEMA='gsettings set org.gnome.desktop.interface'
    ${SCHEMA} gtk-theme "$THEME"
    ${SCHEMA} color-scheme "$PREF"
    set_configH "gtk-theme-name" $THEME $GTK3_SETTING_FILE
    set_configH "gtk-theme-name" $THEME $GTK4_SETTING_FILE
    set_configH "gtk-application-prefer-dark-theme" $GPD $GTK3_SETTING_FILE
    set_configH "gtk-application-prefer-dark-theme" $GPD $GTK4_SETTING_FILE

}

# ---------------------------------------------------------
topTitle() {
    clear
    calculate_position
    gum style \
        --foreground 215 --border-foreground 212 --border rounded \
        --align center --width $_W --margin "0 1" --padding "1 1" \
        "$1"
}

# 2 -----------------------
BorderSize() {
    SEL=$(cat $VAR_FILE | grep "\$BORDER_SIZE" | cut -d= -f2)
    topTitle 'BORDER SIZE' " Ctrl+C=cancel. Enter=accept"
    gum log --structured --level info "Define the window border size in pixels.  default: 1"
    OPT=$(gum choose --height=$H_N "<< Bacck" {0..30} --selected=$SEL)
    if [[ "$OPT" == *"user aborted"* || -z "$OPT" ]]; then
        hyprlandDecoration
    fi
    [[ "$OPT" =~ ^[0-9]+$ ]] && set_varConfig "\$BORDER_SIZE" "$OPT"
    notify-send "Border Size" "$OPT"
    hyprlandDecoration
}

# 3 -----------------------
WindowRounding() {
    SEL=$(cat $VAR_FILE | grep "\$ROUNDING" | cut -d= -f2)
    topTitle 'WINDOW ROUNDING' " Ctrl+C=cancel. Enter=accept"
    gum log --structured --level info "Define rounded corners’ radius in pixels. default: 10"
    OPT=$(gum choose --height=$H_N "<< Bacck" {0..40} --selected=$SEL)
    if [[ "$OPT" == *"user aborted"* || -z "$OPT" ]]; then
        hyprlandDecoration
    fi
    [[ "$OPT" =~ ^[0-9]+$ ]] && set_varConfig "\$ROUNDING" "$OPT"
    notify-send "Window Rounding" "$OPT"
    hyprlandDecoration
}

# 4 -----------------------
ShadowRange() {
    SEL=$(cat $VAR_FILE | grep "\$SHADOW_RANGE" | cut -d= -f2)
    topTitle 'SHADOW RANGE' " Ctrl+C=cancel. Enter=accept"
    gum log --structured --level info "Define shadow range for window shadows. default: 4"
    OPT=$(gum choose --height=$H_N "<< Bacck" {1..30} --selected=$SEL)
    if [[ "$OPT" == *"user aborted"* || -z "$OPT" ]]; then
        hyprlandDecoration
    fi
    [[ "$OPT" =~ ^[0-9]+$ ]] && set_varConfig "\$SHADOW_RANGE" "$OPT"
    notify-send "Shadow Range" "$OPT"
    hyprlandDecoration
}

# 5 -----------------------
BlurSize() {
    SEL=$(cat $VAR_FILE | grep "\$BLUR_SIZE" | cut -d= -f2)
    topTitle 'BLUR SIZE' " Ctrl+C=cancel. Enter=accept"
    gum log --structured --level info "Define the size for the blur effect. default: 8"
    OPT=$(gum choose --height=$H_N "<< Bacck" {0..30} --selected=$SEL)
    if [[ "$OPT" == *"user aborted"* || -z "$OPT" ]]; then
        hyprlandDecoration
    fi
    [[ "$OPT" =~ ^[0-9]+$ ]] && set_varConfig "\$BLUR_SIZE" "$OPT"
    notify-send "Blur Size" "$OPT"
    hyprlandDecoration
}

# 6 -----------------------
BlurPass() {
    SEL=$(cat $VAR_FILE | grep "\$BLUR_PASS" | cut -d= -f2)
    topTitle 'BLUR PASS' " Ctrl+C=cancel. Enter=accept"
    gum log --structured --level info "the amount of passes to perform. default: 1"
    OPT=$(gum choose --height=$H_N "<< Bacck" {0..30} --selected=$SEL)
    if [[ "$OPT" == *"user aborted"* || -z "$OPT" ]]; then
        hyprlandDecoration
    fi
    [[ "$OPT" =~ ^[0-9]+$ ]] && set_varConfig "\$BLUR_PASS" "$OPT"
    notify-send "Blur Pass" "$OPT"
    hyprlandDecoration
}

# 7 -----------------------
LaoutType() {
    SEL=$(cat $VAR_FILE | grep "\$LAYOUT" | cut -d= -f2)
    SEL=$(echo $SEL | xargs)
    topTitle 'LAYOUT TYPE' " Ctrl+C=cancel. Enter=accept"
    gum log --structured --level info "which layout to use. [dwindle/master]. default:dwindle"
    OPT=$(gum choose "<< Bacck" "master" "dwindle" --selected=$SEL)
    if [[ "$OPT" =~ ^(master|dwindle)$ ]]; then
        set_varConfig "\$LAYOUT" "$OPT"
        notify-send "Layout" "$OPT"
    fi
    hyprlandDecoration
}

# 8 -----------------------
ActiveOpacity() {
    topTitle "Active Opacity" "Ctrl+C=cancel. Enter=accept"
    gum log --structured --level info "opacity of active windows. [0.0 - 1.0] "

    ACC=$(gum input --prompt "Active Opacity: " --placeholder "ex:0.85" \
        --prompt.foreground 99 --cursor.foreground 99 --width 40)

    if [[ "$ACC" == *"user aborted"* || -z "$ACC" ]]; then
        hyprlandDecoration
    fi

    if gum confirm --prompt.foreground 82 " Do you want change Active Opacity to $ACC"; then
        set_varConfig "\$ACTIVE_OPACITY" "$ACC"
    fi
    hyprlandDecoration
}

# 9 -----------------------
InactiveOpacity() {
    topTitle "Inactive Opacity" "Ctrl+C=cancel. Enter=accept"
    gum log --structured --level info "opacity of inactive windows. [0.0 - 1.0] "

    ACC=$(gum input --prompt "Opacity: " --placeholder "ex:0.85" \
        --prompt.foreground 99 --cursor.foreground 99 --width 40)

    if [[ "$ACC" == *"user aborted"* || -z "$ACC" ]]; then
        hyprlandDecoration
    fi

    if gum confirm --prompt.foreground 82 " Do you want change Inactive Opacity to $ACC"; then
        set_varConfig "\$INACTIVE_OPACITY" "$ACC"
    fi
    hyprlandDecoration
}

# 10 -----------------------
DimmingStrength() {
    topTitle "Dimming Strength" "Ctrl+C=cancel. Enter=accept"
    gum log --structured --level info "how much inactive windows should be dimmed [0.0 - 1.0] default=0.5"

    ACC=$(gum input --prompt "Opacity: " --placeholder "ex:0.5" \
        --prompt.foreground 99 --cursor.foreground 99 --width 40)

    if [[ "$ACC" == *"user aborted"* || -z "$ACC" ]]; then
        hyprlandDecoration
    fi

    if gum confirm --prompt.foreground 82 " Do you want change Dimming Strength to $ACC"; then
        set_varConfig "\$DIM_STRENGTH" "$ACC"
    fi
    hyprlandDecoration
}
# ------------------- Decoration ---------------------------
hyprlandDecoration() {
    topTitle 'Decorations' " Ctrl+C=cancel. Enter=accept"
    gum log --structured --level info "Select any item or  Ctrl+C=cancel"
    OPT=$(gum choose --height=$H_N "01 << Back ..." \
        "02 Border Size" "03 Window Rounding" \
        "04 Shadow Range" "05 Blur Size" \
        "06 Blur Pass" "07 Laout" \
        "08 Active Opacity" "09 Inactive Opacity" \
        "10 Dimming Strength")
    case $OPT in
    01*) main ;;
    02*) BorderSize ;;
    03*) WindowRounding ;;
    04*) ShadowRange ;;
    05*) BlurSize ;;
    06*) BlurPass ;;
    07*) LaoutType ;;
    08*) ActiveOpacity ;;
    09*) InactiveOpacity ;;
    10*) DimmingStrength ;;
    esac
    main
}

# ---------------------------------------------------------
globalColorsAccent() {
    topTitle "ACCENT COLOR" "Enter=confirm. Ctrl+C=cancel"
    gum log --structured --level info "Accent color for global theme waybar,rofi,border, etc.."

    ACC=$(gum choose --height=$H_N "<< Back ..." \
        "Select" "Red" "Green" "Yellow" \
        "Blue" "Purple" "Cyan" "Orange" \
        "Pink" "Brown" "Teal" "Slate" \
        "Coral" "DeepPink" "Olive" \
        "OrangeRed" "SeaGreen")

    if [[ "$ACC" == *"user aborted"* || "$ACC" == "<< Back"* || -z "$ACC" ]]; then
        main
    elif [[ "$ACC" == "Select" ]]; then
        ACC=$(yad --color)
        #  notify-send "Colors Accent" "color yad #$ACC"
    else
        case $ACC in
        Red) ACC=#E62D42 ;;
        Green) ACC=#3A944A ;;
        Yellow) ACC=#C88800 ;;
        Blue) ACC=#3584E4 ;;
        Purple) ACC=#9141AC ;;
        Cyan) ACC=#00ffff ;;
        Orange) ACC=#ED5B00 ;;
        Pink) ACC=#D56199 ;;
        Brown) ACC=#5E3B3B ;;
        Teal) ACC=#008080 ;;
        Slate) ACC=#6F8396 ;;
        Coral) ACC=#ff7f50 ;;
        DeepPink) ACC=#ff1493 ;;
        Olive) ACC=#808000 ;;
        OrangeRed) ACC=#ff4500 ;;
        SeaGreen) ACC=#2e8b57 ;;
        esac
    fi

    if [[ -z "$ACC" ]]; then
        main
    fi
    # notify-send "Colors Accent" "selrcted  $ACC"

    $path/change_colors.sh $ACC

    main
}

# ---------------------------------------------------------
colorsFromCurrentImage() {
    LIGHT=$1
    File=$(readlink -f ~/.cache/background)
    if [[ ! -f $File ]]; then
        notify-send "error" "image no found: \n $File"
        main
    fi
    topTitle "colors From Current Image" "colorsheme from current Image by pywal"
    if gum confirm "Colorscheme from  $File ?"; then
        # wal -b $BACKGROUND -q -i $File

        wal -q $LIGHT -i $File
        wal --preview
        # $path/change_colors.sh "wal"
        gum spin \
            --title "pywal genirat theme from image..." \
            --title.foreground 99 \
            -- sh -c ' ~/.config/hypr/settings/change_colors.sh wal'

        setgtk_theme $LIGHT
    fi
    main
}

# ---------------------------------------------------------
walThemes() {
    # List=$(ls $PYWAL_THEMES | cut -d. -f1)
    # topTitle 'PYWAL THEMES' " Ctrl+C=cancel. Enter=accept"
    #  gum log --structured --level info "choose theme. from colorsheme"

    OPT=$(
        wal --theme | awk '/-/{print $2}' | fzf --preview-window follow \
            --preview 'wal  -q --theme {} > /tmp/wal.log & ; wal --preview' \
            --bind='ctrl-p:preview(wal --preview)' \
            --preview-window top:5 \
            --header 'CTRL-c or ESC quit, Enter submit' \
            --color 'border:magenta,label:#cccccc' \
            --color 'preview-border:magenta,preview-label:#ccccff'
    )
    if [[ "$OPT" == *"user aborted"* || -z "$OPT" ]]; then
        main
    fi

    wal --preview
    # wal -q -f $OPT
    sleep 0.5
    gum spin --show-stderr \
        --title "pywal genirat theme from $OPT colorshemes..." \
        --title.foreground 99 \
        -- sh -c '~/.config/hypr/settings/change_colors.sh wal'
    sleep 0.5
    #if gum confirm "Do you want to apply the color theme $OPT ?"; then
    #    wal -q -f $OPT
    #    $path/change_colors.sh "wal"
    #
    #else
    #    walThemes
    #fi
    main
}
# ---------------------------------------------------------
walThemes_light() {

    # topTitle 'PYWAL THEMES' " Ctrl+C=cancel. Enter=accept"
    #  gum log --structured --level info "choose theme. from colorsheme"

    OPT=$(
        ls $PYWAL_THEMES | cut -d. -f1 | fzf --preview-window follow \
            --preview 'wal -l -q --theme {} > /tmp/wal.log & ; wal --preview' \
            --bind='ctrl-p:preview(wal --preview)' \
            --preview-window top:5 \
            --header 'CTRL-c or ESC quit, Enter submit' \
            --color 'border:magenta,label:#cccccc' \
            --color 'preview-border:magenta,preview-label:#ccccff'
    )
    if [[ "$OPT" == *"user aborted"* || -z "$OPT" ]]; then
        main
    fi
    wal --preview
    gum spin --show-stderr \
        --title "pywal genirat theme from $OPT colorshemes..." \
        --title.foreground 99 \
        -- sh -c ' ~/.config/hypr/settings/change_colors.sh wal'
    sleep 0.5
    # wal --preview
    # if gum confirm "Do you want to apply the color theme $OPT ?"; then
    #     wal -l -q -f $OPT
    #     $path/change_colors.sh "wal"
    # else
    #     walThemes_light
    # fi
    main
}
# ---------------------------------------------------------
hyprlandOptions() {

    #:-- GET OPTIONS
    [ $(cat $VAR_FILE | grep "\$BLUR_ENABLED" | cut -d= -f2) = true ] && list+="BLUR_ENABLED,"
    [ $(cat $VAR_FILE | grep "\$BLUR_IGNORE_OPACITY" | cut -d= -f2) = true ] && list+="BLUR_IGNORE_OPACITY,"
    [ $(cat $VAR_FILE | grep "\$ANIMATIONS_ENABLED" | cut -d= -f2) = true ] && list+="ANIMATIONS_ENABLED,"
    [ $(cat $VAR_FILE | grep "\$RESIZE_ON_BORDER" | cut -d= -f2) = true ] && list+="RESIZE_ON_BORDER,"
    [ $(cat $VAR_FILE | grep "\$DROP_SHADOW" | cut -d= -f2) = true ] && list+="DROP_SHADOW,"
    [ $(cat $VAR_FILE | grep "\$SHADOW_IGNORE_WINDOW" | cut -d= -f2) = true ] && list+="SHADOW_IGNORE_WINDOW,"
    [ $(cat $VAR_FILE | grep "\$WORKSPACE_SWIPE" | cut -d= -f2) = true ] && list+="WORKSPACE_SWIPE,"
    [ $(cat $VAR_FILE | grep "\$DIM_INACTIVE" | cut -d= -f2) = true ] && list+="DIM_INACTIVE,"
    #list=$(cat ~/.config/hypr/variables.conf | grep "true"  | cut -d= -f1 | tr -d "$")  TODO replace " " by ","
    [ $(cat $VAR_FILE | grep "\$FOLLOW_MOUSE" | cut -d= -f2) = 1 ] && list+="FOLLOW_MOUSE,"
    [ $(cat $VAR_FILE | grep "\$BLUR_WAYBAR" | cut -d= -f2) = blur,waybar ] && list+="BLUR_WAYBAR,"
    #:-- BEGIN GUM
    topTitle 'HYPRLAND OPTIONS' "enable/disable any option  "
    gum log --structured --level info "Space=enable/disable item. Ctrl+C=cancel. Enter=accept "
    OPT=$(gum choose --no-limit --height=$H_N --selected-prefix "[✓] " --unselected-prefix "[ ] " \
        --selected.foreground 82 \
        "BLUR_ENABLED" "BLUR_IGNORE_OPACITY" \
        "RESIZE_ON_BORDER" "DROP_SHADOW" \
        "SHADOW_IGNORE_WINDOW" "ANIMATIONS_ENABLED" \
        "WORKSPACE_SWIPE" "DIM_INACTIVE" "FOLLOW_MOUSE" "BLUR_WAYBAR" --selected=$list)

    notify-send "hyprlandOptions" "$OPT"
    if [[ "$OPT" == *"user aborted"* || -z "$OPT" ]]; then
        main
    fi
    echo " $OPT"
    #:-- SET OPTIONS
    [[ -z $(echo $OPT | grep "BLUR_ENABLED") ]] && set_varConfig "\$BLUR_ENABLED" "false" || set_varConfig "\$BLUR_ENABLED" "true"
    [[ -z $(echo $OPT | grep "BLUR_IGNORE_OPACITY") ]] && set_varConfig "\$BLUR_IGNORE_OPACITY" "false" || set_varConfig "\$BLUR_IGNORE_OPACITY" "true"
    [[ -z $(echo $OPT | grep "ANIMATIONS_ENABLED") ]] && set_varConfig "\$ANIMATIONS_ENABLED" "false" || set_varConfig "\$ANIMATIONS_ENABLED" "true"
    [[ -z $(echo $OPT | grep "RESIZE_ON_BORDER") ]] && set_varConfig "\$RESIZE_ON_BORDER" "false" || set_varConfig "\$RESIZE_ON_BORDER" "true"
    [[ -z $(echo $OPT | grep "DROP_SHADOW") ]] && set_varConfig "\$DROP_SHADOW" "false" || set_varConfig "\$DROP_SHADOW" "true"
    [[ -z $(echo $OPT | grep "SHADOW_IGNORE_WINDOW") ]] && set_varConfig "\$SHADOW_IGNORE_WINDOW" "false" || set_varConfig "\$SHADOW_IGNORE_WINDOW" "true"
    [[ -z $(echo $OPT | grep "WORKSPACE_SWIPE") ]] && set_varConfig "\$WORKSPACE_SWIPE" "false" || set_varConfig "\$WORKSPACE_SWIPE" "true"
    [[ -z $(echo $OPT | grep "DIM_INACTIVE") ]] && set_varConfig "\$DIM_INACTIVE" "false" || set_varConfig "\$DIM_INACTIVE" "true"
    [[ -z $(echo $OPT | grep "FOLLOW_MOUSE") ]] && set_varConfig "\$FOLLOW_MOUSE" "0" || set_varConfig "\$FOLLOW_MOUSE" "1"
    [[ -z $(echo $OPT | grep "BLUR_WAYBAR") ]] && set_varConfig "\$BLUR_WAYBAR" "ignorezero,waybar" || set_varConfig "\$BLUR_WAYBAR" "blur,waybar"

    main
}

#  -----------------------
AnimationSource() {

    List=$(ls ~/.config/hypr/animations/ | grep ".conf")
    SEL=$(cat $VAR_FILE | grep "\$ANIMATIONS_SOURCE" | cut -d= -f2)

    topTitle 'ANIMATION SOURCE' " Ctrl+C=cancel. Enter=accept"
    gum log --structured --level info "predefined animations. default:default.conf"
    OPT=$(gum choose --height=$H_N "<< Bacck" $List --selected=$SEL)
    if [[ "$OPT" == *".conf" ]]; then
        set_varConfig "\$ANIMATIONS_SOURCE" "$OPT"
        notify-send "Animation Source" "$OPT"
    fi
    main
}

# ---------------------------------------------------------
waybarTheme() {

    List=$(ls $WAYBAR_PATH/themes)
    topTitle 'WAYBAR THEME' " Ctrl+C=cancel. Enter=accept"
    gum log --structured --level info "choose theme. default:default.conf"
    OPT=$(gum choose --height=$H_N "<< Back" $List)
    if [[ "$OPT" == *"user aborted"* || "$OPT" == "<< Back" || -z "$OPT" ]]; then
        main
    else
        THEME=$WAYBAR_PATH/themes/$OPT
        if [ ! -f $THEME/config.jsonc ]; then
            notify-send "no found" " $THEME/config.jsonc"
        elif [ ! -f $THEME/style.css ]; then
            notify-send "no found" "$THEME/style.css"
        else
            ln -s -f $THEME/modules.jsonc $WAYBAR_PATH/modules.jsonc
            ln -s -f $THEME/config.jsonc $WAYBAR_PATH/config.jsonc
            ln -s -f $THEME/style.css $WAYBAR_PATH/style.css

            nohup $SCRIPT_Path/statusbar r >/tmp/waybar.log &
            notify-send "waybar Theme" "$OPT"
        fi
    fi
    main
}

# 9 -----------------------
borderMultiColors() {
    ### + ------------------------------
    gum log --structured --level info "multi color for active boeder."
    ACC=$(gum input --prompt "Color: " --placeholder "ex:#00ff00" \
        --prompt.foreground 99 --cursor.foreground 99 --width 20)
    if [[ "$ACC" == *"user aborted"* || -z "$OPT" ]]; then
        main
    fi
    ACC=$(echo $ACC | tr -d '#')
    if ! [[ $ACC =~ ^[0-9A-Fa-f]{6}$ ]]; then
        gum log --structured --level error "Color: $ACC no  valid !!! "
        if gum confirm --prompt.foreground 82 " Do you want to try again?"; then
            borderMultiColors
        else
            main
        fi
    fi
    M_colors=$(cat $VAR_FILE | grep "\$MULTI_COLOR" | cut -d= -f2)
    set_varConfig "\$COL_ACTIVE_BORDER" "rgb($ACC) $M_colors 90deg"

    ### - ------------------------------
    #    [ $(cat $VAR_FILE | grep "\$BORDER_MULTI_COLOR" | cut -d= -f2) = 1 ] && SEL="true" || SEL="false"
    #    topTitle 'Border Multi Colors' " Ctrl+C=cancel. Enter=accept"
    #    gum log --structured --level info "Multi Colors animated border 90 deg"
    #    OPT=$(gum choose "<< Bacck" "true" "false" --selected=$SEL)

    #    COLOR=$(cat $VAR_FILE | grep "\$COL_ACTIVE_BORDER" | cut -d= -f2 | awk '{print $1}')

    #    if [[ "$OPT" == "true" ]]; then
    #        M_colors=$(cat $VAR_FILE | grep "\$MULTI_COLOR" | cut -d= -f2)
    #        set_varConfig "\$BORDER_MULTI_COLOR" "1"
    #        set_varConfig "\$COL_ACTIVE_BORDER" "$COLOR $M_colors 90deg"
    #        notify-send "border multi colors" "Color; $COLOR $M_colors  enabled"
    #    elif [[ "$OPT" == "false" ]]; then
    #        set_varConfig "\$BORDER_MULTI_COLOR" "0"
    #        set_varConfig "\$COL_ACTIVE_BORDER" "$COLOR"
    #        notify-send "border multi colors" "Color; $COLOR  $OPT disabled"
    #    fi
    ### - ------------------------------
    main
}

# ---------------------------------------------------------
hyprCursor() {
    topTitle 'Hyprcursor theme' "Hyprcursor theme"
    OPT=$(gum choose --height=$H_N "<< Back" \
        "Bibata-Modern-Amber" "Bibata-Modern-Classic" \
        "Bibata-Modern-Ice" "Bibata-Original-Amber" \
        "Bibata-Original-Classic" "Bibata-Original-Ice" \
        "Nordzy-cursors-white" "Nordzy-cursors" \
        "Nordzy-catppuccin-macchiato-green")

    if [[ "$OPT" == *"user aborted"* || "$OPT" == "<< Back" || -z "$OPT" ]]; then
        main
    else
        # TODO virify if file exist
        [ -f ~/.icons/$OPT/manifest.hl ] && EXIST=true
        [ -f ~/.local/share/icons/$OPT/manifest.hl ] && EXIST=true
        [ -f /usr/shar/icons/$OPT/manifest.hl ] && EXIST=true
        if [[ $EXIST == true ]]; then
            hyprctl setcursor $OPT 24
            set_varConfig "\$HYPR_CURSOR" $OPT
            gsettings set org.gnome.desktop.interface cursor-theme $OPT
            GTK3_SETTING=~/.config/gtk-3.0/settings.ini
            GTK4_SETTING=~/.config/gtk-4.0/settings.ini
            set_configH "gtk-cursor-theme-name" "$OPT" ${GTK3_SETTING}
            set_configH "gtk-cursor-theme-name" "$OPT" ${GTK4_SETTING}

        fi
    fi
    main
}

# ---------------------------------------------------------
main() {
    topTitle 'SETTINGS' "general settings for hyprland"
    OPT=$(gum choose --height=$H_N "01 Exit ..." \
        "02 Change Colors accent" "03 Dark Theme from current image" \
        "04 Light Theme from current image" "05 Dark Theme from colorsheme" \
        "06 Light Theme from colorsheme" \
        "07 Hyprland Options" "08 Hyprland Decoration" "09 Waybar theme" \
        "10 Animation Source" "11 Border multi colors" "12 Hyprcursor")

    case $OPT in
    01*) exit ;;
    02*) globalColorsAccent ;;
    03*) colorsFromCurrentImage ;;
    04*) colorsFromCurrentImage -l ;;
    05*) walThemes ;;
    06*) walThemes_light ;;
    07*) hyprlandOptions ;;
    08*) hyprlandDecoration ;;
    09*) waybarTheme ;;
    10*) AnimationSource ;;
    11*) borderMultiColors ;;
    12*) hyprCursor ;;

    esac
}

main

exit
