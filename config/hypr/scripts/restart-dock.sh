#!/bin/env bash

killall nwg-dock-hyprland
sleep 1
nwg-dock-hyprland -r  -p bottom -i 28 -w 5 -ml 3 -x \
    -c 'rofi -show drun -theme ~/.config/rofi/styles/style_5'
